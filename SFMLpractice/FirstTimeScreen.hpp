#ifndef FIRSTTIMESCREEN_H
#define FIRSTTIMESCREEN_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class MenuText;
class MenuSprite;

class FirstTimeScreen : public Screen
{
public:
	FirstTimeScreen();
	~FirstTimeScreen() {}

protected:
	virtual void keyPressedEvent(sf::Keyboard::Key);
	virtual void timerTick(float dt);

private:
	void setupSprites();
	void optionSelected();

	bool userHasSelected;
	float selectedTimer;

	vector<MenuSprite*> sprites;
	vector<MenuText*> text;
	MenuText* button;
};

#endif