#include "MenuText.hpp"
#include "MainMenu.hpp"
#include "Resources.hpp"

MenuText::MenuText(Screen* mother, const string& _message)
    : GameSprite(mother, 0, 0)
{
    timer = 0.f;
    songSection = 0;
    musicBPM = 70.f;
    message = _message;
    font = *Resources::font("spaceInvader");
    text.setFont(font);
    text.setCharacterSize(MENU_TEXT_SIZE);
    text.setFillColor(sf::Color(255, 255, 255, 255));
    text.setString(message);

    float tempw = text.getGlobalBounds().width;
    float temph = text.getGlobalBounds().height;
    setDimensions(tempw, temph);
    setPosition((WINDOWX / 2.f) - (width() / 2.f), 0);
    status = IDLE;
}

void MenuText::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    text.setPosition(x, y);
}

void MenuText::display(sf::RenderWindow& App)
{
    App.draw(text);
}

void MenuText::timerTick(float dt)  //mostly handles visual effects
{
    if (status == HOVERING) {   //timed with the music
        timer += dt;
        float timeStamp = Resources::music("menu_music")->getPlayingOffset().asSeconds();
        
        if (songSection == 0 && timeStamp > 27.5f) {
            songSection++;
            timer = 0.f;
            musicBPM = 77.5f;
        } else if (songSection == 1 && timeStamp > 64.65f) {
            songSection++;
            timer = 0.f;
            musicBPM = 70.f;
        } else if (songSection == 2 && timeStamp > 92.1f) {
            songSection++;
            timer = 0.f;
            musicBPM = 77.5f;
        } else if (songSection == 3 && timeStamp < 1.f) {
            songSection = 0;
            timer = 0.f;
            musicBPM = 70.f;
        }

        float temp = (timer * (musicBPM / 60.f)) - static_cast<int>(timer * (musicBPM / 60.f));
        text.setFillColor(sf::Color(255, 255 * temp, 0, 255));
    }
    else if (status == SELECTED) {  //timed with the screen transition time
        timer += dt;
        bool invisible = static_cast<int>(timer * 5) % 2;
        text.setFillColor(sf::Color(255, 255, 255, 255 * invisible));
    }
}

void MenuText::setIdle()
{
    status = IDLE;
    text.setFillColor(sf::Color(255, 255, 255, 255));
}

void MenuText::setHovering()
{
    status = HOVERING;
    timer = 0.f;
}

void MenuText::setSelected()
{
    status = SELECTED;
    timer = 0.f;
}

void MenuText::setTextSize(int size)
{
    text.setCharacterSize(size);
    float tempw = text.getGlobalBounds().width;
    float temph = text.getGlobalBounds().height;
    setDimensions(tempw, temph);
}

void MenuText::setColor(sf::Color color)
{
    text.setFillColor(color);
}

void MenuText::setString(string str)
{
    text.setString(str);
    float tempw = text.getGlobalBounds().width;
    float temph = text.getGlobalBounds().height;
    setDimensions(tempw, temph);
}