#include "IconSprite.hpp"
#include "InvadersGame.hpp"
#include "Resources.hpp"

IconSprite::IconSprite(Screen* mother, char type, int tier)
    : GameSprite(mother, 0, 0)
{
    float tempw = 128.f;
    float temph = 128.f;
    sprite.setScale(sf::Vector2f(4.f, 4.f));
    setDimensions(tempw, temph);

    float tempx = (WINDOWX / 2.f) - (tempw / 2.f);
    float tempy = (WINDOWX / 2.f) - (temph / 2.f) + (128.f + 16.f);
    setPosition(tempx, tempy);

    switch (type)
    {
    case 't': {
        sprite.setTexture(*Resources::texture("bullseye"));
        break;
    }
    case 'd': {
        sprite.setTexture(*Resources::texture("shield"));
        break;
    }
    case 'p': {
        sprite.setTexture(*Resources::texture("pawn"));
        break;
    }
    case 'c': {
        sprite.setTexture(*Resources::texture("hourglass"));
        break;
    }
    default:
        break;
    }

    switch (tier)
    {
    case 0: {
        sprite.setColor(sf::Color(127, 127, 127, 255));
        break;
    }
    case 1: {
        sprite.setColor(sf::Color(255, 64, 0, 255));
        break;
    }
    case 2: {
        sprite.setColor(sf::Color(223, 223, 255, 255));
        break;
    }
    case 3: {
        sprite.setColor(sf::Color(255, 223, 0, 255));
        break;
    }
    default:
        break;
    }

}

void IconSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    sprite.setPosition(x, y);
}

void IconSprite::display(sf::RenderWindow& App)
{
    App.draw(sprite);
}