#include "Screens.hpp"
#include "Resources.hpp"

#if 1

int main ()
{
    Resources::initializeAll();
    ScreenBasic* screenptr;

    sf::RenderWindow App(sf::VideoMode(WINDOWX, WINDOWY), "Cosmic Conquerors");
    App.setFramerateLimit(200);
    Resources::generateBackgroundTexture(App);

    if (Resources::firstTimePlaying()) {
        screenptr = new FirstTimeScreen;
        screenptr->updateFullscreen(App);
        screenptr->run(App);
        delete screenptr;
        screenptr = new MainMenu;
    } else {
        screenptr = new MainMenu;
        screenptr->updateFullscreen(App);
    }

    ScreenCode nextScreen = MENU;
    Resources::music("menu_music")->play();
    Resources::music("menu_music")->setLoop(true);

    while (nextScreen != EXIT) {
        nextScreen = screenptr->run(App);
        delete screenptr;

        switch (nextScreen)
        {
        case MENU:
            screenptr = new MainMenu;
            break;
        case GAME:
            screenptr = new InvadersGame;
            break;
        case LEADERBOARD:
            screenptr = new LeaderboardMenu;
            break;
        case ACHIEVEMENTS:
            screenptr = new AchievementMenu;
            break;
        case TUTORIAL:
            screenptr = new TutorialMenu;
            break;
        case SETTINGS:
            screenptr = new SettingsMenu;
            break;
        case EXIT:
            break;
        default:
            break;
        }
    }

    Resources::cleanUp();
    return 0;
}
#endif

# if 0


int main() 
{
  cout << "\n";

  char a = 'A';
  int b = static_cast<int> (a);

  const char c = a;

  char d = c;

  cout << b;

  cout << "\n";

  return 0;
}

#endif