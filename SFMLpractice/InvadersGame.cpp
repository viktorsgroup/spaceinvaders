#include "InvadersGame.hpp"
#include "Resources.hpp"
#include "BaseSprite.hpp"
#include "BaseBullet.hpp"
#include "InvaderBullet.hpp"
#include "InvaderSprite.hpp"
#include "DefenceBlock.hpp"
#include "LivesLeftPanel.hpp"
#include "ScorePanel.hpp"
#include "ExplosionSprite.hpp"
#include "SaucerSprite.hpp"
#include "MessageSprite.hpp"
#include "IconSprite.hpp"

InvadersGame::InvadersGame() : Screen()
{
    readFromFile();
    pauseTimer = -1.f;
    freezeState = NONE;
    hasLetGoOfSpace = false;
    level = 1;
    levelSpeed = 32.f;
    levelFireSpeed = AVERAGE_BULLET_TIME;
    bulletSpawnTimer = levelFireSpeed;
    saucerSpawnTimer = AVERAGE_SAUCER_TIME;
    bulletCounter = 0;
    untouched = true;
    for (int i = 0; i < 4; i++) {
        achievementIcons[i] = nullptr;
    }
    leftSelected = true;

    base = new BaseSprite(this, WINDOWX / 2.f - 32.f, BASE_Y, BASE_WIDTH, BASE_HEIGHT);
    addSprite(base);
    bullet = nullptr;

    lives = new LivesLeftPanel(this, WINDOWX - BASE_WIDTH - 8.f,
                                WINDOWY - BASE_HEIGHT - 8.f,
                                BASE_WIDTH, BASE_HEIGHT);
    addSprite(lives);

    scoreCounter = new ScorePanel(this, 8.f, WINDOWY - 48.f, 516.f, 42.f);
    addSprite(scoreCounter);

    invaderSpeed = levelSpeed;
    invaderHasHitWall = false;
    numberOfInvaders = 0;

    setupNewBoard();

}

void InvadersGame::keyPressedEvent(sf::Keyboard::Key key)
{
    if (freezeState == PAUSED) {
        if (key == sf::Keyboard::Left) {
            leftSelected = true;
            Resources::sound("boop")->play();
            leftOption->setBoxColor(sf::Color(0, 0, 255, 255));
            rightOption->setBoxColor(sf::Color(0, 0, 0, 255));
        }
        else if (key == sf::Keyboard::Right) {
            leftSelected = false;
            Resources::sound("boop")->play();
            leftOption->setBoxColor(sf::Color(0, 0, 0, 255));
            rightOption->setBoxColor(sf::Color(0, 0, 255, 255));
        }
        else if (key == sf::Keyboard::Escape) {
            Resources::sound("select")->play();
            pause();
        }
        else if (key == sf::Keyboard::Space) {
            Resources::sound("select")->play();
            if (leftSelected == true) {
                pause();
            }
            else {
                Resources::music("game_music1")->stop();
                Resources::music("game_music2")->stop();
                Resources::music("game_music3")->stop();

                Resources::music("menu_music")->play();
                Resources::music("menu_music")->setLoop(true);
                writeToFile();
                changeScreen = true;
                screenQueued = MENU;
            }
        }
    } else if (freezeState == SUBMIT) {
        if (key >= sf::Keyboard::A && key <= sf::Keyboard::Z) {
            Resources::sound("boop")->play();
            char tmp = static_cast<char>(key - sf::Keyboard::A + 'a');
            inputBox->addChar(tmp);
            inputBox->setPosition((WINDOWX / 2.f) - (inputBox->width() / 2.f),
                                   inputBox->yPos());
            if (inputBox->getString().find_first_of('-') == string::npos) {
                inputBox->setBoxColor(sf::Color(0, 0, 255, 255));
            } else {
                inputBox->setBoxColor(sf::Color(0, 0, 0, 255));
            }
        }
        else if (key == sf::Keyboard::BackSpace) {
            Resources::sound("boop")->play();
            inputBox->removeChar();
            inputBox->setPosition((WINDOWX / 2.f) - (inputBox->width() / 2.f),
                                    inputBox->yPos());
            if (inputBox->getString() == "---") {
                inputBox->setBoxColor(sf::Color(0, 0, 0, 0));
            } else {
                inputBox->setBoxColor(sf::Color(0, 0, 0, 255));
            }
        }
        else if (key == sf::Keyboard::Enter &&
            inputBox->getString().find_first_of('-') == string::npos) {
            Resources::sound("select")->play();
            if (worldRecordMessage != nullptr) {
                killSprite(worldRecordMessage);
                worldRecordMessage = nullptr;
            }
            killSprite(messageBox);
            messageBox = nullptr;
            
            saveScore(inputBox->getString(), scoreCounter->getScore());
            killSprite(inputBox);
            inputBox = nullptr;

            killSprite(leftOption);
            leftOption = nullptr;

            setupNewBoard();
            base->setColor(255, 0, 0, 255);
            Screen::unfreeze();
            freezeState = NONE;
            Resources::music("game_music1")->play();
        }
    } else {
        if (key == sf::Keyboard::R) {
            if (freezeState != BOARD_CLEARED) {
                //can't restart before submitting to leaderboard
                if (freezeState != GAME_OVER || highScores.size() < 10 ||
                    scoreCounter->getScore() <= highScores[9].second) {
                    Resources::music("game_music1")->stop();
                    Resources::music("game_music2")->stop();
                    Resources::music("game_music3")->stop();
                    Resources::music("game_music1")->play();
                    changeScreen = true;
                    screenQueued = GAME;
                }
            }
        }
        else if (key == sf::Keyboard::Escape) {
            pause();
        }
        else if (key == sf::Keyboard::Space) {
            baseFireBullet();
        }
        else if (key == sf::Keyboard::Up) {
            setVelocityBasedOnArrowKeys();
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
                baseFireBullet();
            }
        }
        else {
            setVelocityBasedOnArrowKeys();
        }
    }
}

void InvadersGame::keyReleasedEvent(sf::Keyboard::Key key)
{
    setVelocityBasedOnArrowKeys();
}

void InvadersGame::pause()
{
    if (freezeState == PAUSED) {
        pauseTimer = 0.2f;
        killSprite(messageBox);
        messageBox = nullptr;
        killSprite(leftOption);
        leftOption = nullptr;
        killSprite(rightOption);
        rightOption = nullptr;

        Screen::unfreeze();
        freezeState = NONE;
        
        if (Resources::music("game_music1")->getStatus() == 1) {
            Resources::music("game_music1")->play();
        } else if (Resources::music("game_music2")->getStatus() == 1) {
            Resources::music("game_music2")->play();
        } else if (Resources::music("game_music3")->getStatus() == 1) {
            Resources::music("game_music3")->play();
        }
    }
    else if (freezeState == NONE && pauseTimer <= 0.f) {
        messageBox = new MessageSprite(this, "paused", 0.1f);
        addSprite(messageBox);
        Screen::freeze(-1, messageBox);
        leftSelected = true;

        leftOption = new MessageSprite(this, "resume", -1.f);
        addSprite(leftOption);
        leftOption->setPosition(256 - 32 + 8, 512 + 128);
        leftOption->setFontSize(64);
        
        rightOption = new MessageSprite(this, "quit", -1.f);
        addSprite(rightOption);
        rightOption->setPosition(512 + 128 - 16, 512 + 128);
        rightOption->setFontSize(64);
        rightOption->setBoxColor(sf::Color(0, 0, 0, 255));
        
        freezeState = PAUSED;
        if (Resources::music("game_music1")->getStatus() == 2) {
            Resources::music("game_music1")->pause();
        } else if (Resources::music("game_music2")->getStatus() == 2) {
            Resources::music("game_music2")->pause();
        } else if (Resources::music("game_music3")->getStatus() == 2) {
            Resources::music("game_music3")->pause();
        }
    }
}

void InvadersGame::gameOver()
{
    Resources::music("game_music1")->stop();
    Resources::music("game_music2")->stop();
    Resources::music("game_music3")->stop();

    level = 1;
    messageBox = new MessageSprite(this, "GAME OVER", 0.5f);
    addSprite(messageBox);
    Screen::freeze(6000, messageBox);
    freezeState = GAME_OVER;
    bulletCounter = 0;
    untouched = true;
}

int InvadersGame::getRank(int score)
{
    if (score != 0) {
        for (int i = 0; i < 10; i++) {
            if ((highScores.size() == i) ||
                (score > highScores[i].second)) {
                return i + 1;
            }
        }
        return 0;
    } else {
        return 0;
    }
}

void InvadersGame::checkAchievements()
{
    if (level == 1 && targetPractice < 3) {
        if (bulletCounter <= TP_GOLD) {
            targetPractice = 3;
            achievementAnimation('t', 3);
        } else if (bulletCounter <= TP_SILVER && targetPractice < 2) {
            targetPractice = 2;
            achievementAnimation('t', 2);
        } else if (bulletCounter <= TP_BRONZE && targetPractice < 1) {
            targetPractice = 1;
            achievementAnimation('t', 1);
        }
    }
    if (untouched && defensiveTactics < 3) {
        if (level == DT_GOLD - 1) {
            defensiveTactics = 3;
            achievementAnimation('d', 3);
        } else if (level == (DT_SILVER - 1) && defensiveTactics < 2) {
            defensiveTactics = 2;
            achievementAnimation('d', 2);
        } else if (level == (DT_BRONZE - 1) && defensiveTactics < 1) {
            defensiveTactics = 1;
            achievementAnimation('d', 1);
        }
    }
    if (level == 1 && patientOpening < 3) {
        if (scoreCounter->getScore() >= PO_GOLD) {
            patientOpening = 3;
            achievementAnimation('p', 3);
        } else if (scoreCounter->getScore() >= PO_SILVER && patientOpening < 2) {
            patientOpening = 2;
            achievementAnimation('p', 2);
        } else if (scoreCounter->getScore() >= PO_BRONZE && patientOpening < 1) {
            patientOpening = 1;
            achievementAnimation('p', 1);
        }
    }
    if (clutchEndgame < 3) {
        int greatestClutch = 0;
        for (auto &i : clutchVector) {
            if (i.first > greatestClutch) {
                greatestClutch = i.first;
            }
        }
        clutchVector.clear();
        if (greatestClutch >= CE_GOLD) {
            clutchEndgame = 3;
            achievementAnimation('c', 3);
        } else if (greatestClutch >= CE_SILVER && clutchEndgame < 2) {
            clutchEndgame = 2;
            achievementAnimation('c', 2);
        } else if (greatestClutch >= CE_BRONZE && clutchEndgame < 1) {
            clutchEndgame = 1;
            achievementAnimation('c', 1);
        }
    }
}

void InvadersGame::achievementAnimation(char type, int tier)
{
    writeToFile();  //in case the user quick restarts, achievements must be saved
    for (int i = 0; i < 4; i++) {
        if (achievementIcons[i] == nullptr) {
            achievementIcons[i] = new IconSprite(this, type, tier);
            addSprite(achievementIcons[i]);
            // spaces out icons if multiple on screen at once
            for (int j = i - 1; j >= 0; j--) {
                achievementIcons[i]->setPosition(achievementIcons[i]->xPos()
                + (ACHIEVEMENT_ICON_SPACING / 2.f), achievementIcons[i]->yPos());
                achievementIcons[j]->setPosition(achievementIcons[j]->xPos()
                - (ACHIEVEMENT_ICON_SPACING / 2.f), achievementIcons[j]->yPos());
            }
            break;
        }
    }
}

void InvadersGame::saveScore(string str, int score)
{
    pair<string, int> tempPair = pair<string, int>(str, score);
    if (score != 0) {
        for (int i = 0; i < 10; i++) {
            if ((highScores.size() == i) || (score > highScores[i].second)) {
                highScores.insert(highScores.begin() + i, tempPair);
                if (highScores.size() > 10) {
                    highScores.pop_back();
                }
                break;
            }
        }
    }
    writeToFile();
}

void InvadersGame::writeToFile()
{
    getSettingsData();
    ofstream saveFile("save.txt", std::ofstream::out | std::ofstream::trunc);
    if (saveFile.is_open()) {
        saveFile << isFullscreen << '\n';
        saveFile << isMusicMuted << '\n';
        saveFile << isSFXMuted << '\n';
        saveFile << targetPractice << '\n';
        saveFile << defensiveTactics << '\n';
        saveFile << patientOpening << '\n';
        saveFile << clutchEndgame << '\n';

        for (auto& vec : highScores) {
            saveFile << vec.first << '\n';
            saveFile << vec.second << '\n';
        }
    }
}

void InvadersGame::readFromFile()
{
    ifstream saveFile("save.txt");
    if (saveFile.is_open()) {
        saveFile.ignore(6);
        saveFile >> targetPractice;     saveFile.ignore();
        saveFile >> defensiveTactics;   saveFile.ignore();
        saveFile >> patientOpening;     saveFile.ignore();
        saveFile >> clutchEndgame;      saveFile.ignore();

        while (!saveFile.eof()) {
            pair<string, int> tempPair;
            saveFile >> tempPair.first;     saveFile.ignore();
            saveFile >> tempPair.second;    saveFile.ignore();
            highScores.push_back(tempPair);
        }
        highScores.pop_back();
        while (highScores.size() < 10) {
            highScores.push_back(pair<string, int>("---", 0));
        }
    }
}

void InvadersGame::boardCleared()
{
    checkAchievements();
    level += 1;
    messageBox = new MessageSprite(this, "LEVEL " + to_string(level), 0.3f);
    addSprite(messageBox);
    Screen::freeze(4000, messageBox);
    freezeState = BOARD_CLEARED;

    if (level == 3) {
        Resources::music("game_music1")->stop();
    }
    else if (level == 5) {
        Resources::music("game_music2")->stop();
    }
}

void InvadersGame::setVelocityBasedOnArrowKeys() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        base->setVelocity(0, 0);
    } else {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                base->setVelocity(0, 0);
            }
            else {
                base->setVelocity(-256.f, 0);
            }
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            base->setVelocity(256.f, 0);
        }
        else {
            base->setVelocity(0, 0);
        }
    }
}

void InvadersGame::baseFireBullet()
{
    if (bullet == nullptr && freezeState == NONE && pauseTimer <= 0.f
        && (hasLetGoOfSpace || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))) {
        bullet = new BaseBullet(this, base->xPos() + base->width() / 2 - 1.f,
            base->yPos() - 8.f);
        addSprite(bullet);
        Resources::sound("pew")->play();
        bulletCounter++;
    }
    hasLetGoOfSpace = false;
}

void InvadersGame::invaderHitWall()
{
    invaderHasHitWall = true;
    firstRowCleared = true;
}

float InvadersGame::invaderVelocity()
{
    return invaderSpeed;
}

void InvadersGame::timerTick(float dt)
{
    Screen::timerTick(dt);

    if (pauseTimer > 0.f) {
        pauseTimer -= dt;
        if (pauseTimer <= 0.f && sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            hasLetGoOfSpace = true;
            baseFireBullet();
        }
    }

    if (!hasLetGoOfSpace) {
        checkSpaceDrop();
    }

    if (invaderHasHitWall) {
        invertInvaderMarch(dt);
        invaderHasHitWall = false;
    }
    
    if (bullet != nullptr)
        checkBaseBulletHit();

    if (freezeState != BOARD_CLEARED) {
        auto id = ibullets.begin();
        while (id != ibullets.end()) {
            if (checkForInvaderBulletHits(*id)) {
                killIBullet(*id++);
            }
            else {
                id++;
            }
        }
    }

    if (!Screen::isInFreeze()) {
        invaderBulletRandomFire(dt);
        if (firstRowCleared && saucer == nullptr) {
            randomSaucerSpawn(dt);
        }
        for (auto &i : clutchVector) {   //advancing clutch timers
            i.second -= dt;
        }
    }
}

void InvadersGame::checkSpaceDrop()
{
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        hasLetGoOfSpace = true;
    }
}

void InvadersGame::invertInvaderMarch(float dt)
{
    bool rightWallCollision = false;
    if (invaderSpeed > 0.f) {
        rightWallCollision = true;
    }
    invaderSpeed = -invaderSpeed;
    float horizontalShift;

    int collidingColumn;    //all this setup to cancel accumulating horizontal shift
    if (rightWallCollision) {
        for (int j = 0; j < 11; j++) {
            for (int i = 0; i < 5; i++) {
                if (invaders[i][j] != nullptr) {
                    collidingColumn = j;
                }

            }
        }
    } else {
        for (int j = 10; j > -1; j--) {
            for (int i = 0; i < 5; i++) {
                if (invaders[i][j] != nullptr) {
                    collidingColumn = j;
                }

            }
        }
    }
    
    bool deathByCollision = false;
    float overlap = -1.f;

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 11; j++) {
            if (j == collidingColumn) {
                horizontalShift = 0.f;
            } else {
                horizontalShift = 0.5f * dt * invaderSpeed;
            }

            if (invaders[i][j] != nullptr) {
                invaders[i][j]->setPosition(invaders[i][j]->xPos() + horizontalShift,
                    invaders[i][j]->yPos() + INVADERS_JUMP_Y);

                // checking for overlap between invader and blocks
                overlap = invaders[i][j]->yPos() + invaders[i][j]->height() -
                                                                blocks[0]->yPos();
                if (overlap > 0) {
                    if (overlap > 128) {
                        overlap = 128.f;
                    }
                    for (int k = 0; k < 4; k++) {
                        blocks[k]->cropTop(overlap / 4.f);
                    }

                    // checking for overlap between invader and base
                    overlap = invaders[i][j]->yPos() + invaders[i][j]->height() - base->yPos();
                    if (overlap > 0) {
                        deathByCollision = true;
                    }
                }
            }
        }
    }
    if (deathByCollision == true) {
        gameOver();
    }
}

void InvadersGame::setupNewBoard()
{
    // Music
    if (level == 3) {
        Resources::music("game_music2")->play();
    } else if (level == 5) {
        Resources::music("game_music3")->play();
    }

    // invaders spawn
    int rowsSkipped = level - 1;
    if (level > 6) {
        rowsSkipped = 5;
    }
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 11; j++) {
            killSprite(invaders[i][j]);
            invaders[i][j] = new InvaderSprite(this, j * INVADERS_JUMP_Y,
                16.f + (i + rowsSkipped) * INVADERS_JUMP_Y, i);
            addSprite(invaders[i][j]);
        }
    }
    numberOfInvaders = 55;

    // blocks spawn
    for (int i = 0; i < 4; i++) {
        killSprite(blocks[i]);
        blocks[i] = new DefenceBlock(this, 64.f + i * BLOCK_DISTANCE * 2,
            WINDOWY - BLOCK_DISTANCE * 2);
        addSprite(blocks[i]);

        if (level == 2) {
            blocks[i]->cropTop(16);
        }
        else if (level == 3) {
            blocks[i]->cropTop(24);
        }
        else if (level == 4) {
            blocks[i]->cropTop(28);
        }
        else if (level == 5) {
            blocks[i]->cropTop(30);
        }
        else if (level >= 6) {
            blocks[i]->cropTop(32);
        } 
    }

    // fire rate
    levelFireSpeed = AVERAGE_BULLET_TIME;
    for (int i = 1; i < level && i < 6; i++) {
        levelFireSpeed -= 0.1f;
    }

    // speed (only thing that scales after lvl 6)
    levelSpeed = 32.f;
    for (int i = 1; i < level; i++) {
        levelSpeed += 16.f;
    }
    invaderSpeed = levelSpeed;

    // other setup
    base->setPosition((WINDOWX / 2.f) - (base->width() / 2.f), BASE_Y);
    killSprite(saucer);
    saucer = nullptr;
    killAllBullets();
    bulletSpawnTimer = levelFireSpeed;
    hasLetGoOfSpace = false;

    if (level == 1) {
        lives->restore();
        resetScore();
        firstRowCleared = false;
        saucerSpawnTimer = AVERAGE_SAUCER_TIME;
    }
}

void InvadersGame::killAllBullets()
{
    killBaseBullet();
    auto id = ibullets.begin();
    while (id != ibullets.end()) {
        killIBullet(*id++);
    }
}

void InvadersGame::killSaucer()
{
    killSprite(saucer);
    saucer = nullptr;
}

void InvadersGame::checkBaseBulletHit()
{
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 11; j++) {
            if (invaders[i][j] != nullptr && invaders[i][j]->hitBy(bullet)) {

                killAndExplode(invaders[i][j]);
                invaders[i][j] = nullptr;
                killBaseBullet();
                Resources::sound("boom")->play();
                raiseScore(INVADERPOINTS);

                clutchVector.push_back(std::pair<int, float>(0, CLUTCH_WINDOW));
                for (auto &i : clutchVector) {
                    if (i.second > 0.f) {
                        i.first += 1;
                    }
                }

                numberOfInvaders--;
                if (numberOfInvaders == 0) {
                    boardCleared();
                    return;
                }

                if (invaderSpeed > 0) {
                    invaderSpeed = levelSpeed * (60.f / (numberOfInvaders + 5));
                } else {
                    invaderSpeed = -levelSpeed * (60.f / (numberOfInvaders + 5));
                }
                return;
            }
        }
    }
    for (int i = 0; i < 4; i++) {
        if (blocks[i] != nullptr && blocks[i]->hitBy(bullet)) {
            blocks[i]->damagedByBullet(bullet);
            killBaseBullet();
            Resources::sound("boom")->play();
            return;
        }
    }
    if (saucer != nullptr && saucer->hitBy(bullet)) {
        killAndExplode(saucer);
        saucer = nullptr;
        killBaseBullet();
        Resources::sound("boom")->play();
        raiseScore(SAUCERPOINTS);
        return;
    }
}

void InvadersGame::raiseScore(int n) {
    scoreCounter->raiseScore(n);
    if (scoreCounter->getScore() >= (extraLivesGained + 1) * EXTRALIFEPOINTS) {
        extraLivesGained += 1;
        lives->gainALife();
        Resources::sound("1-up")->play();
    }
}

void InvadersGame::resetScore() {
    scoreCounter->setScore(0);
    extraLivesGained = 0;
}

bool InvadersGame::checkForInvaderBulletHits(InvaderBullet* ib)
{
    if (base->hitBy(ib)) {

        killAndExplode(base);
        base = new BaseSprite(this, WINDOWX / 2.f - 32.f, BASE_Y, BASE_WIDTH, BASE_HEIGHT);
        addSprite(base);
        base->setColor(0, 0, 0, 0);

        freezeState = BASE_DESTROYED;
        Resources::sound("boom")->play();
        Screen::freeze(500);

        untouched = false;
        lives->looseALife();
        if (lives->livesLeft() < 1) {
            gameOver();
        }
        return true;
    }

    for (int i = 0; i < 4; i++) {
        if (blocks[i] != nullptr && blocks[i]->hitBy(ib)) {
            blocks[i]->damagedByBullet(ib);
            Resources::sound("boom")->play();
            return true;
        }
    }
    return false;
}

void InvadersGame::freezeTimeout(GameSprite* exception)
{
    if (freezeState == BASE_DESTROYED) {
        base->setColor(255, 0, 0, 255);
        killAllBullets();
        freezeState = NONE;
    }
    else if (freezeState == GAME_OVER) {
        killSprite(messageBox);
        messageBox = nullptr;

        int tempRank = getRank(scoreCounter->getScore());
        if (tempRank) {

            inputBox = new MessageSprite(this, "A--", -1.f); //avoids '-' size jank
            addSprite(inputBox);
            inputBox->setFontSize(128);
            inputBox->setPosition((WINDOWX / 2.f) - (inputBox->width() / 2.f),
                (WINDOWX / 2.f) - (inputBox->height() / 2.f));
            inputBox->setBoxColor(sf::Color(0, 0, 0, 0));

            messageBox = new MessageSprite(this,
                "you are number " + to_string(tempRank) +
                "!\nplease enter name", 0.1f);
            addSprite(messageBox);
            messageBox->setFontSize(64);
            messageBox->setPosition((WINDOWX / 2.f) - (messageBox->width() / 2.f),
                inputBox->yPos() - messageBox->height() - 32);

            // bad practice, reusing a badly named pointer
            leftOption = new MessageSprite(this, "press enter to confirm", 0.f);
            addSprite(leftOption);
            leftOption->setFontSize(64);
            leftOption->setPosition((WINDOWX / 2.f) - (leftOption->width() / 2.f),
                inputBox->yPos() + inputBox->height() + 32);

            if (scoreCounter->getScore() > WORLD_HIGH_SCORE) {
                worldRecordMessage = new MessageSprite(this,
                    "you beat the world\n"
                    "record! contact me!", -1.0f);
                addSprite(worldRecordMessage);
                worldRecordMessage->setFontSize(64);
                worldRecordMessage->setPosition((WINDOWX / 2.f) -
                    (worldRecordMessage->width() / 2.f),
                    inputBox->yPos() - messageBox->height() -
                    worldRecordMessage->height() - 64);
                worldRecordMessage->setBoxColor(sf::Color(255, 0, 0, 255));
            }
            inputBox->removeChar(); //absolute jank

            Screen::freeze(-1, messageBox);
            freezeState = SUBMIT;
        } else {
            setupNewBoard();
            base->setColor(255, 0, 0, 255);
            freezeState = NONE;
            Resources::music("game_music1")->play();
        }  
    }
    else if (freezeState == BOARD_CLEARED) {
        setupNewBoard();
        freezeState = NONE;

        killSprite(messageBox);
        messageBox = nullptr;

        for (int i = 0; i < 4; i++) {
            killSprite(achievementIcons[i]);
            achievementIcons[i] = nullptr;
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        baseFireBullet();
    }
}

void InvadersGame::invaderBulletRandomFire(float dt)
{
    bulletSpawnTimer -= dt;
    if (bulletSpawnTimer < 0) {
        invaderBulletFire();
        float rng = Resources::randomNumberBetweenOneAndZero();
        rng -= 0.5f;
        bulletSpawnTimer = levelFireSpeed + (rng * levelFireSpeed);
    }
}

void InvadersGame::randomSaucerSpawn(float dt)
{
    saucerSpawnTimer -= dt;
    if (saucerSpawnTimer < 0) {
        saucer = new SaucerSprite(this, -SAUCERWIDTH, 16);
        addSprite(saucer);
        float rng = Resources::randomNumberBetweenOneAndZero();
        rng -= 0.5f;
        saucerSpawnTimer = AVERAGE_SAUCER_TIME + (rng * AVERAGE_SAUCER_TIME);
    }
}

void InvadersGame::invaderBulletFire()
{
    int rowid;
    int columnid;
    do {
        rowid = 5 * Resources::randomNumberBetweenOneAndZero();
        columnid = 11 * Resources::randomNumberBetweenOneAndZero();
    } while (invaders[rowid][columnid] == nullptr);

    for (int i = rowid + 1; i < 5; i++) {
        if (invaders[i][columnid] != nullptr) {
            rowid = i;
        }
    }

    ibullets.push_back(new InvaderBullet(this, invaders[rowid][columnid]->xPos() +
                invaders[rowid][columnid]->width() / 2.f - 1.f,
                invaders[rowid][columnid]->yPos() +
                invaders[rowid][columnid]->height()));
    addSprite(ibullets.back());
    Resources::sound("pew")->play();
}

void InvadersGame::killBaseBullet()
{
    killSprite(bullet);
    bullet = nullptr;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        baseFireBullet();
}

void InvadersGame::killIBullet(InvaderBullet* ib)
{
    killSprite(ib);
    ibullets.remove(ib);
}

void InvadersGame::explosionSpriteTimeout(ExplosionSprite* sprite)
{
    killSprite(sprite);
}

void InvadersGame::killAndExplode(GameSprite* sprite)
{
    ExplosionSprite* temp = new ExplosionSprite(this, sprite->xPos(), sprite->yPos(),
                sprite->width(), sprite->width() * (2/3.f), 400);
    addSprite(temp);
    killSprite(sprite);
}

void InvadersGame::killAndExplode(InvaderSprite* sprite)
{
    ExplosionSprite* temp = new ExplosionSprite(this, sprite->displayXPos(),
                sprite->yPos(), sprite->width(), sprite->height(), 400);
    addSprite(temp);
    killSprite(sprite);
}

void InvadersGame::killAndExplode(BaseSprite* sprite)
{
    ExplosionSprite* temp = new ExplosionSprite(this, sprite->xPos() - 24.f,
                                sprite->yPos() - 24.f,
                                96.f, 64.f, 1);
    addSprite(temp);
    killSprite(sprite);
}