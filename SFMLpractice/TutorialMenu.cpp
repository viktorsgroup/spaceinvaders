#include "TutorialMenu.hpp"
#include "Resources.hpp"
#include "MenuText.hpp"
#include "IconSprite.hpp"
#include "MenuSprite.hpp"
#include "LivesLeftPanel.hpp"
#include "ScorePanel.hpp"

TutorialMenu::TutorialMenu() : Screen()
{
	userHasSelected = false;
	selectedTimer = -1.f;
	getAchievementData();

	title = new MenuText(this, "controls");
	addSprite(title);
	title->setColor(sf::Color(0, 255, 0, 255));
	title->setTextSize(64);
	title->setPosition(title->xPos(), 32);

	button = new MenuText(this, "next");
	addSprite(button);
	button->setPosition(button->xPos(), 1024 - 96);
	button->setHovering();

	page = 0;
	nextPage();
}

void TutorialMenu::keyPressedEvent(sf::Keyboard::Key key)
{
	if (!userHasSelected && (key == sf::Keyboard::Space)) {
		Resources::sound("select")->play();
		optionSelected();
	}
}

void TutorialMenu::optionSelected()
{
	userHasSelected = true;
	selectedTimer = 0.75f;
	button->setSelected();
}

void TutorialMenu::timerTick(float dt)
{
	Screen::timerTick(dt);

	if (userHasSelected) {
		selectedTimer -= dt;
		if (selectedTimer < 0.f) {
			userHasSelected = false;
			button->setHovering();
			nextPage();
		}
	}
}

void TutorialMenu::getAchievementData()
{
	int temp[4] = { 0, 0, 0, 0 };
	ifstream saveFile("save.txt");
	if (saveFile.is_open()) {
		saveFile.ignore(6);
		saveFile >> temp[0];		saveFile.ignore();
		saveFile >> temp[1];		saveFile.ignore();
		saveFile >> temp[2];		saveFile.ignore();
		saveFile >> temp[3];
	}
	int min = 3;
	for (int i = 0; i < 4; i++) {
		if (temp[i] < min) {
			min = temp[i];
		}
	}
	achievementLowest = min;
}

void TutorialMenu::nextPage()	//wall of text
{
	MenuText* tempt[8];
	MenuSprite* temps[6];
	page++;
	switch (page)
	{
	case 1: {
		tempt[0] = new MenuText(this, "move");
		tempt[1] = new MenuText(this, "fire");
		tempt[2] = new MenuText(this, "holding enables full auto\nbut disables movement");

		tempt[3] = new MenuText(this, "pause");
		tempt[4] = new MenuText(this, "quick restart");

		for (int i = 0; i < 3; i++) {
			addSprite(tempt[i]);
			text.push_back(tempt[i]);
			tempt[i]->setTextSize(32);
			tempt[i]->setPosition(384, 268 + (96 * i));
		}

		for (int i = 3; i < 5; i++) {
			addSprite(tempt[i]);
			text.push_back(tempt[i]);
			tempt[i]->setTextSize(32);
			tempt[i]->setPosition(384, 368 + (96 * i));
		}

		temps[0] = new MenuSprite(this, 288, 256, 64, 64, "arrow");
		temps[0]->rotateSprite('r');
		temps[1] = new MenuSprite(this, 216, 256, 64, 64, "arrow");
		temps[1]->rotateSprite('l');
		temps[2] = new MenuSprite(this, 98, 352, 64, 256, "space");
		temps[3] = new MenuSprite(this, 288, 448, 64, 64, "arrow");

		temps[4] = new MenuSprite(this, 288, 640, 64, 64, "esc");
		temps[5] = new MenuSprite(this, 288, 736, 64, 64, "r");

		for (int i = 0; i < 6; i++) {
			addSprite(temps[i]);
			sprites.push_back(temps[i]);
		}

		break;
	}
	case 2: {
		title->setString("objective");
		title->setPosition((WINDOWX / 2.f) - (title->width() / 2.f), title->yPos());

		auto it = text.begin();
		while (it != text.end()) {
			killSprite(*it);
			it = text.erase(it);
		}

		auto it2 = sprites.begin();
		while (it2 != sprites.end()) {
			killSprite(*it2);
			it2 = sprites.erase(it2);
		}

		tempt[0] = new MenuText(this, "Destroy the aliens before they\nreach the surface");
		tempt[1] = new MenuText(this, "Dodge their fire and don't run\nout of lives");
		tempt[2] = new MenuText(this, "Destroy wandering aliens for\nextra points");
		tempt[3] = new MenuText(this, "You gain an extra life every\n5000 points");

		for (int i = 0; i < 4; i++) {
			addSprite(tempt[i]);
			text.push_back(tempt[i]);
			tempt[i]->setTextSize(32);
			tempt[i]->setPosition(128, 196 + (160 * i));
		}

		tempt[4] = new MenuText(this, "aliens");
		tempt[4]->setPosition(384, 196);
		tempt[4]->setColor(sf::Color(0, 255, 0, 255));

		tempt[5] = new MenuText(this, "lives");
		tempt[5]->setPosition(264, 400);
		tempt[5]->setColor(sf::Color(255, 255, 0, 255));

		tempt[6] = new MenuText(this, "wandering aliens");
		tempt[6]->setPosition(304, 516);
		tempt[6]->setColor(sf::Color(255, 0, 255, 255));

		tempt[7] = new MenuText(this, "points");
		tempt[7]->setPosition(232, 720);
		tempt[7]->setColor(sf::Color(255, 255, 0, 255));

		for (int i = 4; i < 8; i++) {
			addSprite(tempt[i]);
			text.push_back(tempt[i]);
			tempt[i]->setTextSize(32);
		}

		temps[0] = new MenuSprite(this, 848, 220, 48, 32, "sprite2");
		temps[0]->setColor(sf::Color(0, 255, 0, 255));
		addSprite(temps[0]);
		sprites.push_back(temps[0]);

		life = new LivesLeftPanel(this, 840, 396, BASE_WIDTH, BASE_HEIGHT);
		life->looseALife();
		life->looseALife();
		addSprite(life);

		temps[1] = new MenuSprite(this, 840, 540, 64, 32, "saucer");
		temps[1]->setColor(sf::Color(255, 0, 255, 255));
		addSprite(temps[1]);
		sprites.push_back(temps[1]);

		score = new ScorePanel(this, 840, 700, 128, 32);
		score->setScore(100);
		addSprite(score);

		break;
	}
	case 3: {
		button->setString("back");
		title->setString("advanced tips");
		title->setPosition((WINDOWX / 2.f) - (title->width() / 2.f), title->yPos());

		auto it = text.end() - 1;
		while (text.size() > 4) {
			killSprite(*it);
			text.erase(it);
			it = text.end() - 1;
		}

		auto it2 = sprites.begin();
		while (it2 != sprites.end()) {
			killSprite(*it2);
			it2 = sprites.erase(it2);
		}
		killSprite(life);
		life = nullptr;
		killSprite(score);
		score = nullptr;

		if (achievementLowest > 2) {
			text[0]->setString("Now that you have all achievements,\n"
								"why not go for the world record? ;)");
			text[0]->setColor(sf::Color(255, 255, 0, 255));
		} else {
			text[0]->setString("Hunting for achievements is a\n"
				"great way to improve");
		}

		if (achievementLowest > 0) {
			text[1]->setString("Destroy the aliens column by column to\n"
				"increase the distance they must travel");
			text[1]->setColor(sf::Color(255, 0, 255, 255));
		} else {
			text[1]->setString("Get all bronze achievements\n"
				"to unlock this tip");
			text[1]->setColor(sf::Color(0, 255, 255, 255));
		}
		
		if (achievementLowest > 1) {
			text[2]->setString("The aliens descend the slowest when\n"
				"there are exactly 8 full columns");
			text[2]->setColor(sf::Color(255, 0, 255, 255));
		}
		else {
			text[2]->setString("Get all silver achievements\n"
				"to unlock this tip");
			text[2]->setColor(sf::Color(0, 255, 255, 255));
		}
		if (achievementLowest > 2) {
			text[3]->setString("The time between each wandering alien is\n"
				"random, but the countdown carries over\n"
				"from level to level");
			text[3]->setColor(sf::Color(255, 0, 255, 255));
		}
		else {
			text[3]->setString("Get all gold achievements\n"
				"to unlock this tip");
			text[3]->setColor(sf::Color(0, 255, 255, 255));
		}
		
		break;
	}
	case 4: {
		screenQueued = MENU;
		changeScreen = true;
		break;
	}
	default:
		break;
	}
}