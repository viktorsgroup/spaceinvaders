#include "GameSprite.hpp"
#include "Screen.hpp"

GameSprite::GameSprite(Screen*m,float _x, float _y)
{
    mom = m;
    x = _x;
    y = _y;
    xVel = 0;
    yVel = 0;
}

void GameSprite::setPosition(float _x, float _y)
{
    if (_x < 0) {
        setX(0);
        wallHit(LEFT_WALL);
        return;
    } else if (_x + width() > WINDOWX) {
        setX(WINDOWY - width());
        wallHit(RIGHT_WALL);
        return;
    } else {
        setX(_x);
    }
    if (_y < 0) {
        setY(0);
        wallHit(TOP_WALL);
        return;
    } else if (_y + height() > WINDOWX) {
        setY(WINDOWY - height());
        wallHit(BOTTOM_WALL);
        return;
    } else {
        setY(_y);
    }
}

void GameSprite::setVelocity(float _xVel, float _yVel)
{
    xVel = _xVel;
    yVel = _yVel;
}

bool GameSprite::hitBy(GameSprite* other)
{
    if ((x < (other->xPos() + other->width()))
        && (x + w > other->xPos())
        && (y < (other->yPos() + other->height()))
        && (y + h > other->yPos()) )
    {
        return true;
    } else { return false; }
}


void GameSprite::timerTick(float dt)
{
    setPosition(xPos() + xVelocity()*dt, yPos() + yVelocity()*dt);
}

void GameSprite::wallHit(WallType w)
{
    if (w == LEFT_WALL || w == RIGHT_WALL) {
        xVel = -xVel;
    } else {
        yVel = -yVel;
    }
}