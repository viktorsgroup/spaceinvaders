#ifndef ICONSPRITE_HPP
#define ICONSPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class IconSprite : public GameSprite
{
public:
	IconSprite(Screen* mother, char type, int tier);
	~IconSprite() {}

	virtual void setPosition(float x, float y);
	virtual void display(sf::RenderWindow& App);

protected:

private:
	sf::Sprite sprite;
};

#endif