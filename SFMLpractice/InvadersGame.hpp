#ifndef INVADERSGAME_H
#define INVADERSGAME_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class BaseSprite;
class BaseBullet;
class LivesLeftPanel;
class ScorePanel;
class InvaderBullet;
class InvaderSprite;
class DefenceBlock;
class ExplosionSprite;
class SaucerSprite;
class MessageSprite;
class IconSprite;

class InvadersGame : public Screen
{
public:
    InvadersGame();
    ~InvadersGame(){}
    void killBaseBullet();
    void killIBullet(InvaderBullet* ib);
    void invaderHitWall();
    float invaderVelocity();
    void explosionSpriteTimeout(ExplosionSprite*);
    void killSaucer();
protected:
    virtual void keyPressedEvent(sf::Keyboard::Key);
    virtual void keyReleasedEvent(sf::Keyboard::Key);
    virtual void timerTick(float dt);
    virtual void freezeTimeout(GameSprite*);
private:
    void setVelocityBasedOnArrowKeys();
    void baseFireBullet();
    void invertInvaderMarch(float dt);
    void setupNewBoard();
    void checkBaseBulletHit();
    bool checkForInvaderBulletHits(InvaderBullet* ib);
    void invaderBulletRandomFire(float dt);
    void randomSaucerSpawn(float dt);
    void invaderBulletFire();
    void killAndExplode(GameSprite*);
    void killAndExplode(InvaderSprite*);
    void killAndExplode(BaseSprite*);
    void raiseScore(int n);
    void resetScore();
    void pause();
    void gameOver();
    void boardCleared();
    void killAllBullets();
    void checkAchievements();
    void writeToFile();
    void readFromFile();
    void achievementAnimation(char type, int tier);
    void checkSpaceDrop();

    int getRank(int score);
    void saveScore(string str, int score);

    BaseSprite* base;
    BaseBullet* bullet;
    LivesLeftPanel* lives;
    ScorePanel* scoreCounter;
    
    std::list<InvaderBullet*> ibullets;
    bool hasLetGoOfSpace;

    float invaderSpeed;
    bool invaderHasHitWall;

    InvaderSprite* invaders[5][11];
    int numberOfInvaders;

    DefenceBlock* blocks[4];
    int numberOfBlocks;

    SaucerSprite* saucer;
    bool firstRowCleared; //condition for saucer spawning

    int extraLivesGained;

    enum FreezeTimeoutState { NONE, BASE_DESTROYED, PAUSED,
                                GAME_OVER, BOARD_CLEARED, SUBMIT };
    FreezeTimeoutState freezeState;

    MessageSprite* messageBox;
    MessageSprite* leftOption;
    MessageSprite* rightOption;
    MessageSprite* worldRecordMessage;
    MessageSprite* inputBox;

    bool leftSelected;  //pause menu management
    float pauseTimer;   //prevents pause abuse

    int level;
    float levelSpeed;
    float levelFireSpeed;

    float bulletSpawnTimer;
    float saucerSpawnTimer;

    int targetPractice;
    int defensiveTactics;
    int patientOpening;
    int clutchEndgame;

    vector<pair<string, int>> highScores;

    int bulletCounter;
    bool untouched;
    vector<pair<int, float>> clutchVector;

    IconSprite* achievementIcons[4];
};


#endif