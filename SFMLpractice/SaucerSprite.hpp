#ifndef SAUCERSPRITE_HPP
#define SAUCERSPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class SaucerSprite : public GameSprite
{
public:
    SaucerSprite(InvadersGame* mother, float x, float y);
    ~SaucerSprite() {}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
    virtual bool hitBy(GameSprite*);

protected:
    virtual void wallHit(WallType);
private:
    sf::Sprite sprite;
};

#endif
