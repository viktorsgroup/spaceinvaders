#ifndef MENUSPRITE_HPP
#define MENUSPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class MenuSprite : public GameSprite
{
public:
	MenuSprite(Screen* mother, float x, float y,
		float w, float h, string imagename);
	~MenuSprite() {}

	virtual void setPosition(float x, float y);
	virtual void display(sf::RenderWindow& App);
	void rotateSprite(char direction);
	void setColor(sf::Color color);

protected:

private:
	sf::Sprite sprite;
};

#endif