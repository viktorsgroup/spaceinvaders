#ifndef RESOURCES_HPP
#define RESOURCES_HPP

#include "InvadersGlobal.hpp"

class Resources;

class Resources
{
public:
    static sf::Image* image(string);
    static sf::Texture* texture(string);
    static sf::Sound* sound(string);
    static sf::Music* music(string);
    static sf::Font* font(string);

    static void generateBackgroundTexture(sf::RenderWindow& App);
    static float randomNumberBetweenOneAndZero();
    static bool firstTimePlaying();
    static void initializeAll();
    static void initializeImagesAndTextures();
    static void initializeSounds();
    static void initializeFonts();
    static void cleanUp();

private:
    static bool initialized;
    static map<string, sf::Image*> images;
    static map<string, sf::Texture*> textures;
    static list<sf::SoundBuffer*> soundBuffers;
    static map<string, sf::Sound*> sounds;
    static map<string, sf::Music*> songs;
    static map<string, sf::Font*> fonts;
};

#endif
