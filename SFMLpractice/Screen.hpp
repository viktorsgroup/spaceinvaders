#ifndef SCREEN_HPP
#define SCREEN_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"
#include "ScreenBasic.hpp"

class Screen : public ScreenBasic
{
public:
    Screen();
    ~Screen(){}

    virtual ScreenCode run(sf::RenderWindow &App);
    virtual void updateFullscreen(sf::RenderWindow& App);

    //int width(){return w;}
    //int height(){return h;}

protected:
    bool fullscreenNeedsUpdating;
    void adjustMainViewRatio(sf::RenderWindow& App);

    ScreenCode screenQueued;
    bool changeScreen;

    //sf::RenderWindow *window(){return win;}
    virtual void keyPressedEvent(sf::Keyboard::Key) {}
    virtual void keyReleasedEvent(sf::Keyboard::Key) {}
    void addSprite(GameSprite*);
    virtual void killSprite(GameSprite*);
    virtual void timerTick(float dt);
    virtual void display(sf::RenderWindow& App);
    virtual void beforeClosing() {}

    void freeze(int milliseconds = -1, GameSprite* exception = nullptr);
    virtual void freezeTimeout(GameSprite*) {}
    void unfreeze();
    bool isInFreeze();
    void removeSprite(GameSprite*);

    bool isFullscreen;
    bool isMusicMuted;
    bool isSFXMuted;

    void setMuteMusic(bool on);
    void setMuteSFX(bool on);
    void getSettingsData();

private:
    void setupViews();
    void executeSprites();

    list<GameSprite*> sprites;
    list<GameSprite*> deathRow;

    bool isFrozen;
    float countdown;
    GameSprite* freezeException;

    sf::View* mainView;
    sf::View* background;
    sf::Sprite* backgroundSprite;
    sf::RectangleShape* blackSquare;
};

#endif
