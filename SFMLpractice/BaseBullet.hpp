#ifndef BASEBULLET_HPP
#define BASEBULLET_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class BaseBullet : public GameSprite
{
public:
    BaseBullet(InvadersGame* mother, float x, float y);
    ~BaseBullet() {}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
protected:
    virtual void wallHit(WallType);
private:
    sf::ConvexShape baseBulletShape;
};

#endif
