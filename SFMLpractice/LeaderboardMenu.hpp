#ifndef LEADERBOARDMENU_H
#define LEADERBOARDMENTMENU_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class MenuText;

class LeaderboardMenu : public Screen
{
public:
	LeaderboardMenu();
	~LeaderboardMenu() {}

protected:
	virtual void keyPressedEvent(sf::Keyboard::Key);
	virtual void timerTick(float dt);

private:
	void getLeaderboardData();
	void optionSelected();

	bool userHasSelected;
	float selectedTimer;

	vector<pair<string, int>> entry;
	vector<MenuText*> text;
	MenuText* title;
	MenuText* button;
};

#endif