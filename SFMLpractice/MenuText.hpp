#ifndef MENUTEXT_HPP
#define MENUTEXT_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class MainMenu;

class MenuText : public GameSprite
{
public:
	MenuText(Screen* mother, const string& _message);
	~MenuText() {}

	virtual void setPosition(float x, float y);
	virtual void display(sf::RenderWindow& App);
	virtual void timerTick(float dt);

	void setIdle();
	void setHovering();
	void setSelected();

	void setTextSize(int size);
	void setColor(sf::Color color);
	void setString(string str);
private:
	sf::Text text;
	sf::Font font;
	string message;
	float timer;
	int songSection;
	float musicBPM;

	enum TextStatus { IDLE, HOVERING, SELECTED };
	TextStatus status;

};

#endif