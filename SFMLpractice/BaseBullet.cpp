#include "BaseBullet.hpp"
#include "InvadersGame.hpp"

BaseBullet::BaseBullet(InvadersGame* m, float x, float y)
    : GameSprite(m, x, y)
{
    const float w = 2.f, h = 8.f;
    setDimensions(w, h);
    baseBulletShape.setPointCount(4);
    baseBulletShape.setPoint(0, sf::Vector2f(0, 0));
    baseBulletShape.setPoint(1, sf::Vector2f(0, h));
    baseBulletShape.setPoint(2, sf::Vector2f(w, h));
    baseBulletShape.setPoint(3, sf::Vector2f(w, 0));

    baseBulletShape.setPosition(x, y);
    baseBulletShape.setFillColor(sf::Color::Red);

    setVelocity(0.f, -1024.f);
}



void BaseBullet::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    baseBulletShape.setPosition(xPos(), yPos());
}

void BaseBullet::display(sf::RenderWindow& App)
{
    App.draw(baseBulletShape);
}

void BaseBullet::wallHit(WallType w)
{
    dynamic_cast<InvadersGame*>(mother())->killBaseBullet();
}