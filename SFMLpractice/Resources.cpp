#include "Resources.hpp"
#include <random>
std::random_device rnddev;
std::mt19937 gen(rnddev()); //rng, dist(gen) gives float between 0 and 1
std::uniform_real_distribution<float> dist(0, 1);

bool Resources::initialized = false;
map<string, sf::Image*> Resources::images;
map<string, sf::Texture*> Resources::textures;
list<sf::SoundBuffer*> Resources::soundBuffers;
map<string, sf::Sound*> Resources::sounds;
map<string, sf::Music*> Resources::songs;
map<string, sf::Font*> Resources::fonts;


sf::Image* Resources::image(string name)
{
    return images[name];
}

sf::Texture* Resources::texture(string name)
{
    return textures[name];
}

sf::Sound* Resources::sound(string name)
{
    return sounds[name];
}

sf::Music* Resources::music(string name)
{
    return songs[name];
}

sf::Font* Resources::font(string name)
{
    return fonts[name];
}

void Resources::generateBackgroundTexture(sf::RenderWindow& App)
{
    sf::CircleShape* temp;
    vector<sf::CircleShape*> vec;
    float rng;
    float tempw = App.getSize().x;
    float temph = App.getSize().y;
    int amountOfStars = (tempw * temph) / 5000;

    sf::RenderTexture rt;
    rt.create(tempw, temph);
    rt.clear(sf::Color(16, 16, 16, 255));

    for (int i = 0; i < amountOfStars; i++) {
        rng = 0.5f + (1.5f * randomNumberBetweenOneAndZero());
        temp = new sf::CircleShape(rng);
        temp->setFillColor(sf::Color(255, 255, 255,
            255 * randomNumberBetweenOneAndZero()));
        temp->setPosition(tempw * randomNumberBetweenOneAndZero(),
            temph * randomNumberBetweenOneAndZero());
        vec.push_back(temp);
    }
    for (vector<sf::CircleShape*>::iterator it = vec.begin(); it != vec.end(); ++it) {
        rt.draw(**it);
    }

    sf::Texture* tex = new sf::Texture(rt.getTexture());
    textures["background"] = tex;
}


float Resources::randomNumberBetweenOneAndZero()
{
    return dist(gen);
}

bool Resources::firstTimePlaying()
{
    fstream saveFile;
    saveFile.open("save.txt");
    if (!saveFile) {
        saveFile.open("save.txt", ofstream::out);
        saveFile << 0 << '\n';
        saveFile << 0 << '\n';
        saveFile << 0 << '\n';
        saveFile << 0 << '\n';
        saveFile << 0 << '\n';
        saveFile << 0 << '\n';
        saveFile << 0 << '\n';
        saveFile.close();
        return true;
    } else {
        saveFile.close();
        return false;
    }
}

void Resources::initializeAll()
{
    if (initialized) { return; }
    initializeImagesAndTextures();
    initializeSounds();
    initializeFonts();
    initialized = true;
}

void Resources::initializeImagesAndTextures()
{
    string path = RESOURCE_PATH + "textures\\";
    sf::Texture* temp;
    sf::Image* img;

    temp = new sf::Texture;
    temp->loadFromFile(path + "sprite0.png");
    textures["sprite0"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "sprite1.png");
    textures["sprite1"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "sprite2.png");
    textures["sprite2"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "explosion0.png");
    textures["explosion0"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "explosion1.png");
    textures["explosion1"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "explosion2.png");
    textures["explosion2"] = temp;

    img = new sf::Image;
    temp = new sf::Texture;
    img->loadFromFile(path + "cobblestone.png");
    images["cobblestone"] = img;
    temp->loadFromImage(*img);
    textures["cobblestone"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "saucer.png");
    textures["saucer"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "bullseye.png");
    textures["bullseye"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "shield.png");
    textures["shield"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "pawn.png");
    textures["pawn"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "hourglass.png");
    textures["hourglass"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "arrow.png");
    textures["arrow"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "space.png");
    textures["space"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "esc.png");
    textures["esc"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "r.png");
    textures["r"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "box.png");
    textures["box"] = temp;

    temp = new sf::Texture;
    temp->loadFromFile(path + "check.png");
    textures["check"] = temp;
}

void Resources::initializeSounds()
{
    sf::SoundBuffer* temp;
    sf::Sound* temp2;
    sf::Music* temp3;
    string path = RESOURCE_PATH + "sounds\\";

    temp = new sf::SoundBuffer;
    temp->loadFromFile(path + "pew.wav");
    soundBuffers.push_back(temp);

    temp2 = new sf::Sound;
    temp2->setBuffer(*temp);
    sounds["pew"] = temp2;
    sounds["pew"]->setVolume(10.f);

    temp = new sf::SoundBuffer;
    temp->loadFromFile(path + "boom.wav");
    soundBuffers.push_back(temp);

    temp2 = new sf::Sound;
    temp2->setBuffer(*temp);
    sounds["boom"] = temp2;
    sounds["boom"]->setVolume(10.f);

    temp = new sf::SoundBuffer;
    temp->loadFromFile(path + "1-up.wav");
    soundBuffers.push_back(temp);

    temp2 = new sf::Sound;
    temp2->setBuffer(*temp);
    sounds["1-up"] = temp2;
    sounds["1-up"]->setVolume(30.f);

    temp = new sf::SoundBuffer;
    temp->loadFromFile(path + "boop.wav");
    soundBuffers.push_back(temp);

    temp2 = new sf::Sound;
    temp2->setBuffer(*temp);
    sounds["boop"] = temp2;
    sounds["boop"]->setVolume(10.f);

    temp = new sf::SoundBuffer;
    temp->loadFromFile(path + "select.wav");
    soundBuffers.push_back(temp);

    temp2 = new sf::Sound;
    temp2->setBuffer(*temp);
    sounds["select"] = temp2;
    sounds["select"]->setVolume(20.f);


    temp3 = new sf::Music;
    temp3->openFromFile(path + "menu_music.wav");
    songs["menu_music"] = (temp3);
    songs["menu_music"]->setVolume(10.f);

    temp3 = new sf::Music;
    temp3->openFromFile(path + "game_music1.wav");
    songs["game_music1"] = (temp3);
    songs["game_music1"]->setVolume(15.f);

    temp3 = new sf::Music;
    temp3->openFromFile(path + "game_music2.wav");
    songs["game_music2"] = (temp3);
    songs["game_music2"]->setVolume(15.f);

    temp3 = new sf::Music;
    temp3->openFromFile(path + "game_music3.wav");
    songs["game_music3"] = (temp3);
    songs["game_music3"]->setVolume(15.f);
}

void Resources::initializeFonts()
{
    sf::Font* temp;
    string path = RESOURCE_PATH + "fonts\\";

    temp = new sf::Font;
    temp->loadFromFile(path + "space_invaders.ttf");
    fonts["spaceInvader"] = temp;
}

void Resources::cleanUp()
{
    for (auto temp : images) {
        delete temp.second;
    }
    images.clear();
    for (auto temp : textures) {
        delete temp.second;
    }
    textures.clear();

    for (auto temp : soundBuffers) {
        delete temp;
    }
    soundBuffers.clear();

    for (auto temp : sounds) {
        delete temp.second;
    }
    sounds.clear();
}