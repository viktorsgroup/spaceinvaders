#ifndef SETTINGSMENU_H
#define SETTINGSMENU_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class MenuText;
class MenuSprite;
class MessageSprite;

class SettingsMenu : public Screen
{
public:
	SettingsMenu();
	~SettingsMenu() {}

protected:
	virtual void keyPressedEvent(sf::Keyboard::Key);
	virtual void timerTick(float dt);

private:
	void optionSelected();
	bool toggleSprite(int index, bool pre);
	void deleteFile();
	void setupConfirmMenu();
	void removeConfirmMenu();

	bool confirmMenu;
	bool leftSelected;
	bool userHasSelected;
	float selectedTimer;
	int hoveringOver;
	int numOptions;

	MessageSprite* messageBox;
	MessageSprite* leftOption;
	MessageSprite* rightOption;

	vector<MenuText*> textOptions;
	vector<MenuSprite*> sprites;

	MenuText* title;
};

#endif