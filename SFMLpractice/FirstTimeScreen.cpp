#include "FirstTimeScreen.hpp"
#include "Resources.hpp"
#include "MenuText.hpp"
#include "MenuSprite.hpp"

FirstTimeScreen::FirstTimeScreen() : Screen()
{
	userHasSelected = false;
	selectedTimer = -1.f;

	button = new MenuText(this, "press space to continue");
	addSprite(button);
	button->setPosition(button->xPos(), 1024 - 96);
	button->setHovering();

	setupSprites();
}

void FirstTimeScreen::setupSprites()
{
	MenuText* tempt[2];
	MenuSprite* temps[5];

	tempt[0] = new MenuText(this, "navigate menu");
	tempt[1] = new MenuText(this, "select option");
	for (int i = 0; i < 2; i++) {
		addSprite(tempt[i]);
		text.push_back(tempt[i]);
		tempt[i]->setTextSize(32);
		tempt[i]->setPosition(512, 396 + (128 * i));
	}

	temps[0] = new MenuSprite(this, 416, 384, 64, 64, "arrow");
	temps[0]->rotateSprite('r');
	temps[1] = new MenuSprite(this, 272, 384, 64, 64, "arrow");
	temps[1]->rotateSprite('l');
	temps[2] = new MenuSprite(this, 344, 384, 64, 64, "arrow");
	temps[2]->rotateSprite('d');
	temps[3] = new MenuSprite(this, 344, 312, 64, 64, "arrow");
	temps[4] = new MenuSprite(this, 224, 512, 256, 64, "space");
	for (int i = 0; i < 5; i++) {
		addSprite(temps[i]);
		sprites.push_back(temps[i]);
	}
}

void FirstTimeScreen::keyPressedEvent(sf::Keyboard::Key key)
{
	if (!userHasSelected && (key == sf::Keyboard::Space)) {
		Resources::sound("select")->play();
		optionSelected();
	}
}

void FirstTimeScreen::optionSelected()
{
	userHasSelected = true;
	selectedTimer = 0.75f;
	button->setSelected();
}

void FirstTimeScreen::timerTick(float dt)
{
	Screen::timerTick(dt);

	if (userHasSelected) {
		selectedTimer -= dt;
		if (selectedTimer < 0.f) {
			screenQueued = MENU;
			changeScreen = true;
		}
	}
}