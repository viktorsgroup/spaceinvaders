#include "InvaderSprite.hpp"
#include "InvadersGame.hpp"
#include "Resources.hpp"

InvaderSprite::InvaderSprite(InvadersGame* m, float x, float y, int row)
    : GameSprite(m, x, y)
{
    float tempw = INVADERWIDTH;
    float temph = INVADERHEIGHT;
    setDimensions(tempw, temph);
    setPosition(x, y);

    sprites[0].setTexture(*Resources::texture("sprite0"));
    sprites[1].setTexture(*Resources::texture("sprite1"));
    sprites[2].setTexture(*Resources::texture("sprite2"));

    for (int i = 0; i < 3; i++)
    {
        sprites[i].setScale(sf::Vector2f(4.f, 4.f));
        
        if (row < 2) {
            sprites[i].setColor(sf::Color(0, 255, 0, 255));
        } else if (row < 4) {
            sprites[i].setColor(sf::Color(0, 255, 255, 255));
        } else {
            sprites[i].setColor(sf::Color(255, 0, 255, 255));
        }
        
    }

}

bool InvaderSprite::hitBy(GameSprite* other)
{
    if ((displayXPos() < (other->xPos() + other->width()))
        && (displayXPos() + width() > other->xPos())
        && (yPos() < (other->yPos() + other->height()))
        && (yPos() + height() > other->yPos()))
    {
        return true;
    }
    else { return false; }
}

float InvaderSprite::displayXPos()
{
    int xPosInt = static_cast<int>(xPos()) + INVADERS_JUMP_X / 2.f;
    float temp = xPosInt - xPosInt % INVADERS_JUMP_X;
    return temp;
}

int InvaderSprite::shapeIndex()
{
    int id = displayXPos() / INVADERS_JUMP_X;
    if (id % 2 == 0) { id = 1; }
    else if (id % 4 == 1) { id = 0; }
    else { id = 2; }
    return id;
}

void InvaderSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    sprites[shapeIndex()].setPosition(displayXPos(), y);
}

void InvaderSprite::display(sf::RenderWindow& App)
{
    App.draw(sprites[shapeIndex()]);
}

void InvaderSprite::wallHit(WallType w)
{
    dynamic_cast<InvadersGame*>(mother())->invaderHitWall();
}

float InvaderSprite::xVelocity()
{
    return dynamic_cast<InvadersGame*>(mother())->invaderVelocity();
}