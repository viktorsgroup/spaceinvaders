#include "SaucerSprite.hpp"
#include "InvadersGame.hpp"
#include "Resources.hpp"

SaucerSprite::SaucerSprite(InvadersGame* m, float x, float y)
    : GameSprite(m, x, y)
{
    float tempw = SAUCERWIDTH;
    float temph = INVADERHEIGHT;
    setDimensions(tempw, temph);
    float rng = Resources::randomNumberBetweenOneAndZero();
    if (rng > 0.5f) {
        setVelocity(128.f, 0.f);
        setPosition(-SAUCERWIDTH, 16);
    } else {
        setVelocity(-128.f, 0.f);
        setPosition(WINDOWX, 16);
    }

    sprite.setTexture(*Resources::texture("saucer"));
    sprite.setScale(sf::Vector2f(4.f, 4.f));
    sprite.setColor(sf::Color(255, 0, 255, 255));

}

bool SaucerSprite::hitBy(GameSprite* other)
{
    if ((xPos() < (other->xPos() + other->width()))
        && (xPos() + width() > other->xPos())
        && (yPos() < (other->yPos() + other->height()))
        && (yPos() + height() > other->yPos()))
    {
        return true;
    }
    else { return false; }
}

void SaucerSprite::setPosition(float x, float y)
{
    if (x + width() < 0) {
        setX(0 - width());
        wallHit(LEFT_WALL);
        return;
    }
    else if (x > WINDOWX) {
        setX(WINDOWY);
        wallHit(RIGHT_WALL);
        return;
    }
    else {
        setX(x);
    }
    setY(y);
    sprite.setPosition(xPos(), yPos());
}

void SaucerSprite::display(sf::RenderWindow& App)
{
    App.draw(sprite);
}

void SaucerSprite::wallHit(WallType w)
{
    dynamic_cast<InvadersGame*>(mother())->killSaucer();
}
