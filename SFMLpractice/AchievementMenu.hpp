#ifndef ACHIEVEMENTMENU_H
#define ACHIEVEMENTMENU_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class MenuText;
class IconSprite;

class AchievementMenu : public Screen
{
public:
	AchievementMenu();
	~AchievementMenu() {}

protected:
	virtual void keyPressedEvent(sf::Keyboard::Key);
	virtual void timerTick(float dt);

private:
	void getAchievementData();
	void optionSelected();

	bool userHasSelected;
	float selectedTimer;

	int tpTier;
	int dtTier;
	int poTier;
	int ceTier;

	vector<IconSprite*> icons;
	vector<MenuText*> text;
	MenuText* title;
	MenuText* backButton;
};

#endif