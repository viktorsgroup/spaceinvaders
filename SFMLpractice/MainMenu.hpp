#ifndef MAINMENU_H
#define MAINMENU_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class MenuText;

class MainMenu : public Screen
{
public:
	MainMenu();
	~MainMenu() {}

protected:
	virtual void keyPressedEvent(sf::Keyboard::Key);
	virtual void timerTick(float dt);

private:
	void optionSelected();

	bool userHasSelected;
	float selectedTimer;
	int hoveringOver;
	int numOptions;

	vector<MenuText*> textOptions;

	MenuText* title1;
	MenuText* title2;
	MenuText* title3;
};

#endif