#ifndef LIVESLEFTPANEL_HPP
#define LIVESLEFTPANEL_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class LivesLeftPanel : public GameSprite
{
public:
    LivesLeftPanel(Screen* mother, float x, float y, float w, float h);
    ~LivesLeftPanel() {}

    virtual void display(sf::RenderWindow& App);
    void looseALife();
    void gainALife();
    void restore();
    int livesLeft() { return lLeft; }
private:
    sf::ConvexShape lifeShape;
    int lLeft;
};

#endif
