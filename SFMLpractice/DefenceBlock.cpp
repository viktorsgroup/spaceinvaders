#include "DefenceBlock.hpp"
#include "InvadersGame.hpp"
#include "Resources.hpp"

DefenceBlock::DefenceBlock(InvadersGame* m, float x, float y)
    : GameSprite(m, x, y)
{
    float tempw = 4.f * 32.f;
    float temph = 4.f * 32.f;
    setDimensions(tempw, temph);
    setPosition(x, y);

    image = *Resources::image("cobblestone");
    texture = *Resources::texture("cobblestone");
    sprite.setTexture(texture);
    sprite.setScale(sf::Vector2f(tempw / 128.f, temph / 128.f));
    sprite.setColor(sf::Color(0, 255, 0, 255));
    rowsCropped = 0;
}

bool DefenceBlock::hitBy(GameSprite* other)
{
    if ((xPos() < (other->xPos() + other->width()))
        && (xPos() + width() > other->xPos())
        && (yPos() + (4.f * rowsCropped) < (other->yPos() + other->height()))
        && (yPos() + height() > other->yPos()))
    {
        float temp = other->xPos() + (other->width() / 2.f) - xPos();
        int column = int(temp) / 4;
        if (column == 32) {
            column = 31;
        }

        for (int i = 0; i < 32; i++) {
            if (image.getPixel(column * 4, i * 4) != sf::Color(0, 0, 0, 0)) {
                return true;
            }
        }
        return false;
    }
    else { return false; }
}

void DefenceBlock::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    sprite.setPosition(x, y);
}

void DefenceBlock::display(sf::RenderWindow& App)
{
    App.draw(sprite);
}

void DefenceBlock::damagedByBullet(GameSprite* bullet)
{
    float temp = bullet->xPos() + (bullet->width() / 2.f) - xPos();
    int column = int(temp) / 4;
    if (column == 32) {
        column = 31;
    }
    int row;

    for (int i = 0; i < 32; i++) {
        if (bullet->yVelocity() < 0) {
            row = 31 - i;
        } else {
            row = i;
        }

        if (image.getPixel(column * 4, row * 4) != sf::Color(0, 0, 0, 0)) {
            for (int tempx = column * 4 - 16; tempx < column * 4 + 20; tempx += 4) {
                for (int tempy = row * 4 - 16; tempy < row * 4 + 20; tempy+= 4) {
                    if (tempx >= 0 && tempx < 128 &&
                        tempy >= 0 && tempy < 128) {

                        int rng = 3 + (4.f * Resources::randomNumberBetweenOneAndZero());
                        int dist = abs(tempx / 4 - column) + abs(tempy / 4 - row);
                        if (rng >= dist) {

                            for (int j = 0; j < 4; j++) {
                                for (int k = 0; k < 4; k++) {
                                    image.setPixel(tempx + j, tempy + k,
                                                sf::Color(0, 0, 0, 0));
                                }
                            }
                        }
                    }
                }
            }
            break;
        }
    }
    texture.loadFromImage(image);
}

void DefenceBlock::cropTop(int rows)
{
    for (int tempy = 0; tempy < rows * 4; tempy++) {
        for (int tempx = 0; tempx < 128; tempx++) {
            image.setPixel(tempx, tempy, sf::Color(0, 0, 0, 0));
        }
    }
    texture.loadFromImage(image);
    rowsCropped = rows;
}