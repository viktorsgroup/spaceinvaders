#include "ExplosionSprite.hpp"
#include "InvadersGame.hpp"
#include "Resources.hpp"

ExplosionSprite::ExplosionSprite(InvadersGame* m, float x, float y,
            float w, float h, int ms) : GameSprite(m, x, y)
{
    milliseconds = ms;
    float tempw = w;
    float temph = h;
    setDimensions(tempw, temph);

    sprites[0].setTexture(*Resources::texture("explosion0"));
    sprites[1].setTexture(*Resources::texture("explosion1"));
    sprites[2].setTexture(*Resources::texture("explosion2"));

    for (int i = 0; i < 3; i++) {
        sprites[i].setScale(sf::Vector2f(tempw / 12.f, temph / 8.f));
        sprites[i].setPosition(x, y);
    }
    setPosition(x, y);
}

void ExplosionSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
}

void ExplosionSprite::display(sf::RenderWindow& App)
{
    App.draw(sprites[shapeIndex()]);
}

void ExplosionSprite::timerTick(float dt)
{
    milliseconds -= dt * 1000.f;
    if (milliseconds <= 0) {
        dynamic_cast<InvadersGame*>(mother())->explosionSpriteTimeout(this);
    }
}

int ExplosionSprite::shapeIndex()
{
    int id;
    if (milliseconds > 350) { id = 0; }
    else if (milliseconds > 300) { id = 1; }
    else { id = 2; }
    return id;
}