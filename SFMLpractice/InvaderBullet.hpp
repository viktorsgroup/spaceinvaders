#ifndef INVADERBULLET_HPP
#define INVADERBULLET_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class InvaderBullet : public GameSprite
{
public:
    InvaderBullet(InvadersGame* mother, float x, float y);
    ~InvaderBullet() {}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
protected:
    virtual void wallHit(WallType);
private:
    sf::ConvexShape invaderBulletShape;
};

#endif
