#ifndef CIRCLESPRITE_HPP
#define CIRCLESPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class Screen;

class CircleSprite : public GameSprite
{
public:
    CircleSprite(Screen*mother, float x, float y, float radius = 10);
    ~CircleSprite(){}

    void setRadius(float);
    float radius(){return rad;}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
private:
    sf::CircleShape circleShape;
    float rad;
};

#endif
