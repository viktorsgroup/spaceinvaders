#include "InvaderBullet.hpp"
#include "InvadersGame.hpp"

InvaderBullet::InvaderBullet(InvadersGame* m, float x, float y)
    : GameSprite(m, x, y)
{
    const float w = 2.f, h = 8.f;
    setDimensions(w, h);
    invaderBulletShape.setPointCount(4);
    invaderBulletShape.setPoint(0, sf::Vector2f(0, 0));
    invaderBulletShape.setPoint(1, sf::Vector2f(0, h));
    invaderBulletShape.setPoint(2, sf::Vector2f(w, h));
    invaderBulletShape.setPoint(3, sf::Vector2f(w, 0));

    invaderBulletShape.setPosition(x, y);
    invaderBulletShape.setFillColor(sf::Color::Yellow);

    setVelocity(0.f, 512.f);
}



void InvaderBullet::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    invaderBulletShape.setPosition(xPos(), yPos());
}

void InvaderBullet::display(sf::RenderWindow& App)
{
    App.draw(invaderBulletShape);
}

void InvaderBullet::wallHit(WallType w)
{
    dynamic_cast<InvadersGame*>(mother())->killIBullet(this);
}