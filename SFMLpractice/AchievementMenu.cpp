#include "AchievementMenu.hpp"
#include "Resources.hpp"
#include "MenuText.hpp"
#include "IconSprite.hpp"

AchievementMenu::AchievementMenu() : Screen()
{
	userHasSelected = false;
	selectedTimer = -1.f;
	getAchievementData();

	IconSprite* tempi[4];
	tempi[0] = new IconSprite(this, 't', tpTier);
	tempi[1] = new IconSprite(this, 'd', dtTier);
	tempi[2] = new IconSprite(this, 'p', poTier);
	tempi[3] = new IconSprite(this, 'c', ceTier);

	for (int i = 0; i < 4; i++) {
		addSprite(tempi[i]);
		icons.push_back(tempi[i]);
		tempi[i]->setPosition(64, 164 + (196 * i));
	}

	MenuText* tempt[8];
	tempt[0] = new MenuText(this, "Target\nPractice");
	tempt[1] = new MenuText(this, "Defensive\nTactics");
	tempt[2] = new MenuText(this, "Patient\nOpening");
	tempt[3] = new MenuText(this, "Clutch\nEndgame");

	for (int i = 0; i < 4; i++) {
		addSprite(tempt[i]);
		text.push_back(tempt[i]);
		tempt[i]->setTextSize(32);
		tempt[i]->setPosition(208, 188 + (196 * i));
	}

	// Wall of text ahead!
	for (int i = 4; i < 8; i++) {
		tempt[i] = new MenuText(this, "");
		addSprite(tempt[i]);
		text.push_back(tempt[i]);
		tempt[i]->setTextSize(32);
		tempt[i]->setColor(sf::Color(0, 255, 255, 255));
		tempt[i]->setPosition(448, 164 + (196 * (i - 4)));
	}

	switch (tpTier)
	{
	case 0:
		tempt[4]->setString("Complete level 1 with\n"
			"no more than 65 bullets\n"
			"to unlock bronze tier");
		break;
	case 1:
		tempt[4]->setString("Complete level 1 with\n"
			"no more than 60 bullets\n"
			"to unlock silver tier");
		break;
	case 2:
		tempt[4]->setString("Complete level 1 with\n"
			"no more than 55 bullets\n"
			"to unlock gold tier");
		break;
	case 3:
		tempt[4]->setColor(sf::Color(255, 0, 255, 255));
		tempt[4]->setString("You completed level 1\n"
			"without ever missing!\n"
			"Wow!");
		break;
	default:
		break;
	}

	switch (dtTier)
	{
	case 0:
		tempt[5]->setString("Reach level 2 without\n"
			"losing a single life\n"
			"to unclock bronze tier");
		break;
	case 1:
		tempt[5]->setString("Reach level 3 without\n"
			"losing a single life\n"
			"to unlock silver tier");
		break;
	case 2:
		tempt[5]->setString("Reach level 4 without\n"
			"losing a single life\n"
			"to unlock gold tier");
		break;
	case 3:
		tempt[5]->setColor(sf::Color(255, 0, 255, 255));
		tempt[5]->setString("You rached level 4\n"
			"without getting hit!\n"
			"Wow!");
		break;
	default:
		break;
	}

	switch (poTier)
	{
	case 0:
		tempt[6]->setString("Complete level 1 with\n"
			"at least 6000 points\n"
			"to unclock bronze tier");
		break;
	case 1:
		tempt[6]->setString("Complete level 1 with\n"
			"at least 7000 points\n"
			"to unlock silver tier");
		break;
	case 2:
		tempt[6]->setString("Complete level 1 with\n"
			"at least 8000 points\n"
			"to unlock gold tier");
		break;
	case 3:
		tempt[6]->setColor(sf::Color(255, 0, 255, 255));
		tempt[6]->setString("You collected 8000\n"
			"points in one level!\n"
			"Wow!");
		break;
	default:
		break;
	}

	switch (ceTier)
	{
	case 0:
		tempt[7]->setString("Kill 5 aliens in 2 seconds\n"
			"and survive the level\n"
			"to unlock bronze tier");
		break;
	case 1:
		tempt[7]->setString("Kill 10 aliens in 2 seconds\n"
			"and survive the level\n"
			"to unlock silver tier");
		break;
	case 2:
		tempt[7]->setString("Kill 15 aliens in 2 seconds\n"
			"and survive the level\n"
			"to unlock gold tier");
		break;
	case 3:
		tempt[7]->setColor(sf::Color(255, 0, 255, 255));
		tempt[7]->setString("You destroyed 15 aliens\n"
			"in 2 seconds and survived!\n"
			"Wow!");
		break;
	default:
		break;
	}

	title = new MenuText(this, "achievements");
	addSprite(title);
	title->setColor(sf::Color(0, 255, 0, 255));
	title->setTextSize(64);
	title->setPosition(title->xPos(), 32);

	backButton = new MenuText(this, "back");
	addSprite(backButton);
	backButton->setPosition(backButton->xPos(), 1024 - 96);
	backButton->setHovering();
}

void AchievementMenu::keyPressedEvent(sf::Keyboard::Key key)
{
	if (!userHasSelected && (key == sf::Keyboard::Space)) {
			Resources::sound("select")->play();
			optionSelected();
	}
}

void AchievementMenu::optionSelected()
{
	userHasSelected = true;
	selectedTimer = 0.75f;
	backButton->setSelected();
	screenQueued = MENU;
}

void AchievementMenu::timerTick(float dt)
{
	Screen::timerTick(dt);

	if (userHasSelected) {
		selectedTimer -= dt;
		if (selectedTimer < 0.f) {
			userHasSelected = false;
			changeScreen = true;
		}
	}
}

void AchievementMenu::getAchievementData()
{
	ifstream saveFile("save.txt");
	if (saveFile.is_open()) {
		saveFile.ignore(6);
		saveFile >> tpTier;     saveFile.ignore();
		saveFile >> dtTier;		saveFile.ignore();
		saveFile >> poTier;     saveFile.ignore();
		saveFile >> ceTier;
	}
}