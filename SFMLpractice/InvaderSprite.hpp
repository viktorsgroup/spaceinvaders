#ifndef INVADERSPRITE_HPP
#define INVADERSPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class InvaderSprite : public GameSprite
{
public:
    InvaderSprite(InvadersGame* mother, float x, float y, int row);
    ~InvaderSprite() {}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
    virtual float xVelocity();
    virtual bool hitBy(GameSprite*);
    float displayXPos();

protected:
    virtual void wallHit(WallType);
private:
    sf::Sprite sprites[3];
    int shapeIndex();
};

#endif
