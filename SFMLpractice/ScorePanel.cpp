#include "ScorePanel.hpp"
#include "InvadersGame.hpp"
#include "BaseSprite.hpp"
#include "Resources.hpp"

ScorePanel::ScorePanel(Screen* m, float x, float y, float w, float h)
    : GameSprite(m, x, y)
{
    setDimensions(w, h);
    score = 0;

    font = *Resources::font("spaceInvader");
    scoreCounter.setFont(font);
    scoreCounter.setCharacterSize(h);
    scoreCounter.setFillColor(sf::Color::Yellow);
    scoreCounter.setString(std::to_string(0));
}

void ScorePanel::display(sf::RenderWindow& App)
{
    scoreCounter.setPosition(xPos(), yPos());
    App.draw(scoreCounter);
}

void ScorePanel::setScore(int n) {
    score = n;
    scoreCounter.setString(std::to_string(score));
}

void ScorePanel::raiseScore(int n) {
    score += n;
    scoreCounter.setString(std::to_string(score));
}