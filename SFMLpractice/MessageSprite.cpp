#include "MessageSprite.hpp"
#include "Screen.hpp"
#include "Resources.hpp"

MessageSprite::MessageSprite(Screen* mother, const string& _message,
	float _lettersPerSecond)
    : GameSprite(mother, 0, 0)
{
    timer = 0.f;
    lettersDisplayed = 0;
    message = _message;
    lettersPerSecond = _lettersPerSecond;

    font = *Resources::font("spaceInvader");
    text.setFont(font);
    text.setCharacterSize(MESSAGESIZE);
    text.setFillColor(sf::Color(255, 255, 255, 255));
    text.setString(message);
    //temporarily setting text as full message to get global bounds

    adjustBoxSize();

    float tempx = (WINDOWX / 2.f) - (width() / 2.f);
    float tempy = (WINDOWX / 2.f) - (height() / 2.f);
    setPosition(tempx, tempy);

    box.setFillColor(sf::Color(0, 0, 255, 255));
    if (_lettersPerSecond > 0.f) {
        text.setString("");
    }
}

void MessageSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    box.setPosition(x, y);
    text.setPosition(x, y);
}

void MessageSprite::adjustBoxSize()
{
    float tempw = text.getGlobalBounds().width;
    float temph = (text.getCharacterSize() / 4.f) +
                    text.getGlobalBounds().height;
    box.setSize(sf::Vector2f(tempw, temph));
    setDimensions(tempw, temph);
}

void MessageSprite::setFontSize(int size)
{
    text.setString(message);

    text.setCharacterSize(size);
    adjustBoxSize();

    if (lettersPerSecond > 0.01f) {
        text.setString("");
    }
}

void MessageSprite::addChar(char ch)
{
    string temp = text.getString();
    for (int i = 0; i < 3; i++) {
        if (temp[i] == '-') {
            temp[i] = ch;
            text.setString(temp);
            adjustBoxSize();
            break;
        }
    }
}

void MessageSprite::removeChar()
{
    string temp = text.getString();
    for (int i = 2; i >= 0; i--) {
        if (temp[i] != '-') {
            temp[i] = '-';
            text.setString(temp);
            adjustBoxSize();
            break;
        }
    }
}

string MessageSprite::getString() {
    return text.getString();
}


void MessageSprite::setBoxColor(sf::Color color)
{
    box.setFillColor(color);
}

void MessageSprite::display(sf::RenderWindow& App)
{
    App.draw(box);
    App.draw(text);
}

void MessageSprite::timerTick(float dt)
{
    if (lettersDisplayed < message.length() && lettersPerSecond > 0.f) {
        timer += dt;
        if (timer > (lettersDisplayed + 1) * lettersPerSecond) {
            lettersDisplayed++;
            text.setString(text.getString() +
                message[lettersDisplayed - 1]);
        }
    }
}