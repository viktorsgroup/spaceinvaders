#include "MainMenu.hpp"
#include "Resources.hpp"
#include "MenuText.hpp"

MainMenu::MainMenu() : Screen()
{ 
	userHasSelected = false;
	selectedTimer = -1.f;
	hoveringOver = 0;
	numOptions = 6;

	MenuText* temp[6];
	temp[0] = new MenuText(this, "play");
	temp[1] = new MenuText(this, "leaderboard");
	temp[2] = new MenuText(this, "achievements");
	temp[3] = new MenuText(this, "tutorial");
	temp[4] = new MenuText(this, "settings");
	temp[5] = new MenuText(this, "quit");

	for (int i = 0; i < numOptions; i++) {
		addSprite(temp[i]);
		temp[i]->setPosition(temp[i]->xPos(), 448 + (MENU_TEXT_SIZE * (1.5f * i)));
		textOptions.push_back(temp[i]);
	}
	temp[0]->setHovering();

	title1 = new MenuText(this, "cosmic");
	addSprite(title1);
	title1->setColor(sf::Color(0, 255, 0, 255));
	title1->setTextSize(128);
	title1->setPosition((WINDOWX / 2.f) - (title1->width() / 2.f), 64);

	title2 = new MenuText(this, "conquerors");
	addSprite(title2);
	title2->setColor(sf::Color(0, 255, 0, 255));
	title2->setTextSize(128);
	title2->setPosition((WINDOWX / 2.f) - (title2->width() / 2.f), 196);
	
	title3 = new MenuText(this,
		"entirely legally distinct from the popular game of similar name and gameplay");
	addSprite(title3);
	title3->setColor(sf::Color(255, 255, 255, 127));
	title3->setTextSize(16);
	title3->setPosition((WINDOWX / 2.f) - (title3->width() / 2.f), 368);
}

void MainMenu::keyPressedEvent(sf::Keyboard::Key key)
{
	if (!userHasSelected) {
		if (key == sf::Keyboard::Space) {
			Resources::sound("select")->play();
			optionSelected();
		}
		else if (key == sf::Keyboard::Down) {
			Resources::sound("boop")->play();
			textOptions[hoveringOver]->setIdle();
			hoveringOver++;
			if (hoveringOver >= numOptions) {
				hoveringOver = 0;
			}
			textOptions[hoveringOver]->setHovering();
		}
		else if (key == sf::Keyboard::Up) {
			Resources::sound("boop")->play();
			textOptions[hoveringOver]->setIdle();
			hoveringOver--;
			if (hoveringOver < 0) {
				hoveringOver = numOptions - 1;
			}
			textOptions[hoveringOver]->setHovering();
		}
	}
}


void MainMenu::optionSelected()
{
	userHasSelected = true;
	selectedTimer = 0.75f;
	textOptions[hoveringOver]->setSelected();
	
	switch (hoveringOver)
	{
	case 0: {
		Resources::music("menu_music")->stop();
		screenQueued = GAME;
		break;
	}
	case 1: {
		screenQueued = LEADERBOARD;
		break;
	}
	case 2: {
		screenQueued = ACHIEVEMENTS;
		break;
	}
	case 3: {
		screenQueued = TUTORIAL;
		break;
	}
	case 4: {
		screenQueued = SETTINGS;
		break;
	}
	case 5: {
		screenQueued = EXIT;
		break;
	}
	default:
		break;
	}
}

void MainMenu::timerTick(float dt)
{
	Screen::timerTick(dt);
	
	if (userHasSelected) {
		selectedTimer -= dt;
		if (selectedTimer < 0.f) {
			userHasSelected = false;
			changeScreen = true;		//all options change the screen
			if (hoveringOver == 0) {
				Resources::music("game_music1")->play();
				Resources::music("game_music1")->setLoop(true);
			}
		}
	}
}