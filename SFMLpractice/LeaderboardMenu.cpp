#include "LeaderboardMenu.hpp"
#include "Resources.hpp"
#include "MenuText.hpp"

LeaderboardMenu::LeaderboardMenu() : Screen()
{
	userHasSelected = false;
	selectedTimer = -1.f;
	getLeaderboardData();

	title = new MenuText(this, "leaderboard");
	addSprite(title);
	title->setColor(sf::Color(0, 255, 0, 255));
	title->setTextSize(64);
	title->setPosition(title->xPos(), 32);

	button = new MenuText(this, "back");
	addSprite(button);
	button->setPosition(button->xPos(), 928);
	button->setHovering();

	MenuText* tempworld[3];
	tempworld[0] = new MenuText(this, "World record:");
	tempworld[1] = new MenuText(this, "dev");
	tempworld[2] = new MenuText(this, to_string(WORLD_HIGH_SCORE));

	for (int i = 0; i < 3; i++) {
		addSprite(tempworld[i]);
		text.push_back(tempworld[i]);
		tempworld[i]->setTextSize(32);
		tempworld[i]->setColor(sf::Color(255, 0, 255, 255));

	}
	tempworld[0]->setPosition(196, 164);
	tempworld[1]->setPosition(576, 164);
	tempworld[2]->setPosition(828 - tempworld[2]->width(), 164);

	MenuText* temp[10][3];
	for (int i = 0; i < 10; i++) {
		temp[i][0] = new MenuText(this, to_string(i + 1) + ".");
		temp[i][1] = new MenuText(this, entry[i].first);
		temp[i][2] = new MenuText(this, to_string(entry[i].second));

		for (int j = 0; j < 3; j++) {
			addSprite(temp[i][j]);
			text.push_back(temp[i][j]);
			temp[i][j]->setTextSize(32);

			//color
			if (i == 0) {
				temp[i][j]->setColor(sf::Color(255, 223, 0, 255));
			} else if (i == 1) {
				temp[i][j]->setColor(sf::Color(223, 223, 255, 255));
			} else if (i == 2) {
				temp[i][j]->setColor(sf::Color(255, 64, 0, 255));
			} else if (i < 5) {
				temp[i][j]->setColor(sf::Color(0, 255, 0, 255));
			} else {
				temp[i][j]->setColor(sf::Color(0, 255, 255, 255));
			}
		}
		temp[i][0]->setPosition(196, 260 + (64 * i));
		temp[i][1]->setPosition(576, 260 + (64 * i));
		temp[i][2]->setPosition(828 - temp[i][2]->width(), 260 + (64 * i));
	}
}

void LeaderboardMenu::keyPressedEvent(sf::Keyboard::Key key)
{
	if (!userHasSelected && (key == sf::Keyboard::Space)) {
		Resources::sound("select")->play();
		optionSelected();
	}
}

void LeaderboardMenu::optionSelected()
{
	userHasSelected = true;
	selectedTimer = 0.75f;
	button->setSelected();
}

void LeaderboardMenu::timerTick(float dt)
{
	Screen::timerTick(dt);

	if (userHasSelected) {
		selectedTimer -= dt;
		if (selectedTimer < 0.f) {
			userHasSelected = false;
			screenQueued = MENU;
			changeScreen = true;
		}
	}
}

void LeaderboardMenu::getLeaderboardData()
{
	pair<string, int> temp;

	ifstream saveFile("save.txt");
	if (saveFile.is_open()) {
		saveFile.ignore(6+8);
		while (!saveFile.eof()) {
			pair<string, int> temp;
			saveFile >> temp.first;     saveFile.ignore();
			saveFile >> temp.second;    saveFile.ignore();
			entry.push_back(temp);
		}
		entry.pop_back();
		while (entry.size() < 10) {
			entry.push_back(pair<string, int>("---", 0));
		}
	}
}