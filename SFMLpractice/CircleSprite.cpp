#include "CircleSprite.hpp"
#include "Screen.hpp"

CircleSprite::CircleSprite(Screen*m, float x, float y, float radius)
: GameSprite(m, x, y)
{
    setRadius(radius);
    circleShape.setPosition(x,y);
    circleShape.setFillColor(sf::Color::Red);
}

void CircleSprite::setRadius(float _r)
{
    rad = _r;
    setDimensions(rad*2, rad*2);
    circleShape.setRadius(rad);
}

void CircleSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x,y);
    circleShape.setPosition(xPos(),yPos());
}

void CircleSprite::display(sf::RenderWindow& App)
{
    App.draw(circleShape);
}

