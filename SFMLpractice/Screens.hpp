#ifndef SCREENS_HPP_INCLUDED
#define SCREENS_HPP_INCLUDED

#include "ScreenBasic.hpp"
#include "Screen.hpp"

#include "InvadersGame.hpp"		//ScreenBasic - Screen - InvadersGame
#include "MainMenu.hpp"			//ScreenBasic - Screen - MainMenu
#include "LeaderboardMenu.hpp"	//ScreenBasic - Screen - LeaderboardMenu
#include "AchievementMenu.hpp"	//ScreenBasic - Screen - AchievementMenu
#include "TutorialMenu.hpp"		//ScreenBasic - Screen - TutorialMenu
#include "SettingsMenu.hpp"		//ScreenBasic - Screen - SettingsMenu
#include "FirstTimeScreen.hpp"	//ScreenBasic - Screen - FirstTimeScreen

#endif