#ifndef TUTORIALMENU_H
#define TUTORIALMENTMENU_H

#include "Screen.hpp"
#include "InvadersGlobal.hpp"

class MenuText;
class IconSprite;
class MenuSprite;
class LivesLeftPanel;
class ScorePanel;

class TutorialMenu : public Screen
{
public:
	TutorialMenu();
	~TutorialMenu() {}

protected:
	virtual void keyPressedEvent(sf::Keyboard::Key);
	virtual void timerTick(float dt);

private:
	void getAchievementData();
	void optionSelected();
	void nextPage();

	bool userHasSelected;
	float selectedTimer;
	int page;
	int achievementLowest;

	vector<MenuSprite*> sprites;
	vector<MenuText*> text;
	MenuText* title;
	MenuText* button;
	LivesLeftPanel* life;
	ScorePanel* score;
};

#endif