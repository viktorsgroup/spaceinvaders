#ifndef SCREENBASIC_H
#define SCREENBASIC_H

#include "InvadersGlobal.hpp"

class ScreenBasic
{
public:
    virtual ScreenCode run(sf::RenderWindow& App) = 0;
    virtual void updateFullscreen(sf::RenderWindow& App) = 0;
};

#endif