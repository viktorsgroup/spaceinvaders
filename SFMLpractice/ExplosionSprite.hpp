#ifndef EXPLOSIONSPRITE_HPP
#define EXPLOSIONSPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class ExplosionSprite : public GameSprite
{
public:
    ExplosionSprite(InvadersGame* mother, float x, float y,
                                float h, float w, int ms = -1);
    ~ExplosionSprite() {}

    virtual void setPosition(float, float);
    virtual void display(sf::RenderWindow& App);
    virtual void timerTick(float dt);
private:
    sf::Sprite sprites[3];
    float milliseconds;
    int shapeIndex();
};

#endif
