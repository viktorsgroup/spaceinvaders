#include "BaseSprite.hpp"
#include "InvadersGame.hpp"

BaseSprite::BaseSprite(InvadersGame* m, float x, float y, float w, float h)
    : GameSprite(m, x, y)
{
    setDimensions(w, h);
    setupBaseShape(baseShape);
    
    baseShape.setPosition(x, y);
    baseShape.setFillColor(sf::Color::Red);
}

void BaseSprite::setColor(float r, float g, float b, float a)
{
    baseShape.setFillColor(sf::Color(r, g, b, a));
}


void BaseSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    baseShape.setPosition(xPos(), yPos());
}

void BaseSprite::display(sf::RenderWindow& App)
{
    App.draw(baseShape);
}

void BaseSprite::wallHit(WallType w)
{
    GameSprite::wallHit(w);
    setVelocity(0.f, 0.f);
}

void BaseSprite::setupBaseShape(sf::ConvexShape& cs) {
    int w = BASE_WIDTH;
    int h = BASE_HEIGHT;
    cs.setPointCount(7);
    cs.setPoint(0, sf::Vector2f(0, 0));
    cs.setPoint(1, sf::Vector2f(0, h));
    cs.setPoint(2, sf::Vector2f(w, h));
    cs.setPoint(3, sf::Vector2f(w, 0));
    cs.setPoint(4, sf::Vector2f(w / 2 + h / 2, 0));
    cs.setPoint(5, sf::Vector2f(w / 2, -h / 2));
    cs.setPoint(6, sf::Vector2f(w / 2 - h / 2, 0));
}