#ifndef SCOREPANEL_HPP
#define SCOREPANEL_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class ScorePanel : public GameSprite
{
public:
    ScorePanel(Screen* mother, float x, float y, float w, float h);
    ~ScorePanel() {}

    virtual void display(sf::RenderWindow& App);
    int getScore() { return score; };
    void setScore(int n);
    void raiseScore(int n);

private:
    sf::Text scoreCounter;
    sf::Font font;
    int score;
};

#endif
