#include "SettingsMenu.hpp"
#include "Resources.hpp"
#include "MenuText.hpp"
#include "MenuSprite.hpp"
#include "MessageSprite.hpp"

SettingsMenu::SettingsMenu() : Screen()
{
	confirmMenu = false;

	userHasSelected = false;
	selectedTimer = -1.f;
	hoveringOver = 4;
	numOptions = 5;

	MenuText* temp[5];
	temp[0] = new MenuText(this, "fullscreen");
	temp[1] = new MenuText(this, "mute music");
	temp[2] = new MenuText(this, "mute sfx");
	temp[3] = new MenuText(this, "reset save file");
	temp[4] = new MenuText(this, "back");

	for (int i = 0; i < numOptions; i++) {
		addSprite(temp[i]);
		textOptions.push_back(temp[i]);
	}
	for (int i = 0; i < 3; i++) {
		temp[i]->setPosition(256 + 64, 256 + (MENU_TEXT_SIZE * (1.5f * i)));
	}
	temp[3]->setPosition(512 - (temp[3]->width() / 2.f), 1024 - 384);
	temp[numOptions - 1]->setPosition(temp[numOptions - 1]->xPos(), 1024 - 96);
	temp[numOptions - 1]->setHovering();

	title = new MenuText(this, "settings");
	addSprite(title);
	title->setColor(sf::Color(0, 255, 0, 255));
	title->setTextSize(64);
	title->setPosition((WINDOWX / 2.f) - (title->width() / 2.f), 32);

	MenuSprite* temps[3];
	for (int i = 0; i < 3; i++) {
		temps[i] = new MenuSprite(this, 256 - 32,
			256 + 4 + (MENU_TEXT_SIZE * (1.5f * i)), 64, 64, "box");
		addSprite(temps[i]);
		sprites.push_back(temps[i]);
	}
	toggleSprite(0, !isFullscreen);	//very jank
	toggleSprite(1, !isMusicMuted);
	toggleSprite(2, !isSFXMuted);
}

void SettingsMenu::keyPressedEvent(sf::Keyboard::Key key)
{
	if (confirmMenu) {
		if (key == sf::Keyboard::Left) {
			leftSelected = true;
			Resources::sound("boop")->play();
			leftOption->setBoxColor(sf::Color(0, 0, 255, 255));
			rightOption->setBoxColor(sf::Color(0, 0, 0, 255));
		}
		else if (key == sf::Keyboard::Right) {
			leftSelected = false;
			Resources::sound("boop")->play();
			leftOption->setBoxColor(sf::Color(0, 0, 0, 255));
			rightOption->setBoxColor(sf::Color(0, 0, 255, 255));
		}
		else if (key == sf::Keyboard::Escape) {
			Resources::sound("select")->play();
			removeConfirmMenu();
		}
		else if (key == sf::Keyboard::Space) {
			Resources::sound("select")->play();
			if (leftSelected == true) {
				removeConfirmMenu();
			} else {
				deleteFile();
				removeConfirmMenu();
			}
		}
	}
	else {
		if (!userHasSelected) {
			if (key == sf::Keyboard::Space) {
				Resources::sound("select")->play();
				optionSelected();
			}
			else if (key == sf::Keyboard::Down) {
				Resources::sound("boop")->play();
				textOptions[hoveringOver]->setIdle();
				hoveringOver++;
				if (hoveringOver >= numOptions) {
					hoveringOver = 0;
				}
				textOptions[hoveringOver]->setHovering();
			}
			else if (key == sf::Keyboard::Up) {
				Resources::sound("boop")->play();
				textOptions[hoveringOver]->setIdle();
				hoveringOver--;
				if (hoveringOver < 0) {
					hoveringOver = numOptions - 1;
				}
				textOptions[hoveringOver]->setHovering();
			}
		}
	}
}

bool SettingsMenu::toggleSprite(int index, bool pre)
{
	string spriteName;
	bool tmpbool;
	if (pre) {
		spriteName = "box";
		tmpbool = false;
	} else {
		spriteName = "check";
		tmpbool = true;
	}
	MenuSprite* ptr = new MenuSprite(this, sprites[index]->xPos(),
		sprites[index]->yPos(), 64, 64, spriteName);

	killSprite(sprites[index]);
	sprites[index] = ptr;
	addSprite(ptr);
	return tmpbool;
}

void SettingsMenu::optionSelected()
{
	userHasSelected = true;
	selectedTimer = 0.75f;
	textOptions[hoveringOver]->setSelected();

	switch (hoveringOver)
	{
	case 0: {
		isFullscreen = toggleSprite(0, isFullscreen);
		fullscreenNeedsUpdating = true;
		break;
	}
	case 1: {
		isMusicMuted = toggleSprite(1, isMusicMuted);
		setMuteMusic(isMusicMuted);
		break;
	}
	case 2: {
		isSFXMuted = toggleSprite(2, isSFXMuted);
		setMuteSFX(isSFXMuted);
		break;
	}
	case 3: {
		setupConfirmMenu();
		break;
	}
	case 4: {
		 screenQueued = MENU;
		break;
	}
	default:
		break;
	}
	// write to file
	fstream saveFile("save.txt", std::ios::in | std::ios::out);
	if (saveFile.is_open()) {
		saveFile << isFullscreen << '\n';
		saveFile << isMusicMuted << '\n';
		saveFile << isSFXMuted << '\n';
	}
}

void SettingsMenu::setupConfirmMenu()
{
	confirmMenu = true;
	messageBox = new MessageSprite(this, "are you sure? all your\n"
		"save data will be lost", 0.1f);
	addSprite(messageBox);
	messageBox->setFontSize(64);
	messageBox->setPosition(
		(WINDOWX / 2.f) - (messageBox->width() / 2.f), 1024 - 472);
	leftSelected = true;

	leftOption = new MessageSprite(this, "cancel", -1.f);
	addSprite(leftOption);
	leftOption->setFontSize(64);
	leftOption->setPosition(512 - leftOption->width() - 64, 512 + 256);

	rightOption = new MessageSprite(this, "delete", -1.f);
	addSprite(rightOption);
	rightOption->setFontSize(64);
	rightOption->setPosition(512 + 64, 512 + 256);
	rightOption->setBoxColor(sf::Color(0, 0, 0, 255));
}

void SettingsMenu::removeConfirmMenu()
{
	confirmMenu = false;
	killSprite(messageBox);
	messageBox = nullptr;
	killSprite(leftOption);
	leftOption = nullptr;
	killSprite(rightOption);
	rightOption = nullptr;
}

void SettingsMenu::timerTick(float dt)
{
	Screen::timerTick(dt);

	if (userHasSelected) {
		selectedTimer -= dt;
		if (selectedTimer < 0.f) {
			userHasSelected = false;
			textOptions[hoveringOver]->setHovering();
			if (hoveringOver == numOptions - 1) {
				changeScreen = true;
			}
		}
	}
}

void SettingsMenu::deleteFile()
{
	ofstream saveFile("save.txt", std::ofstream::out | std::ofstream::trunc);
	if (saveFile.is_open()) {
		saveFile << isFullscreen << '\n';
		saveFile << isMusicMuted << '\n';
		saveFile << isSFXMuted << '\n';
		saveFile << 0 << '\n';
		saveFile << 0 << '\n';
		saveFile << 0 << '\n';
		saveFile << 0 << '\n';
	}
}