#ifndef INVADERSGLOBAL_HPP
#define INVADERSGLOBAL_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <list>
#include <string>
#include <fstream>

using namespace std;

enum ScreenCode { MENU, GAME, LEADERBOARD,
				ACHIEVEMENTS, TUTORIAL, SETTINGS, EXIT };

const string RESOURCE_PATH = "assets\\";

const int WINDOWX = 1024;
const int WINDOWY = 1024;
const int BASE_Y = WINDOWY - 64 - 16;
const int BASE_HEIGHT = 24;
const int BASE_WIDTH = 64;
const int INVADERS_JUMP_X = 16;
const int INVADERS_JUMP_Y = 64;
const int BLOCK_DISTANCE = 128;
const int INVADERWIDTH = 4 * 12;
const int INVADERHEIGHT = 4 * 8;
const int SAUCERWIDTH = 4 * 16;
const int INVADERPOINTS = 100;
const int SAUCERPOINTS = 500;
const int EXTRALIFEPOINTS = 5000;
const int MESSAGESIZE = 128;
const float AVERAGE_BULLET_TIME = 1.f;
const float AVERAGE_SAUCER_TIME = 20.f;

// Achievement constants
const int TP_BRONZE = 65, TP_SILVER = 60, TP_GOLD = 55;
const int DT_BRONZE = 2, DT_SILVER = 3, DT_GOLD = 4;
const int PO_BRONZE = 6000, PO_SILVER = 7000, PO_GOLD = 8000;
const int CE_BRONZE = 5, CE_SILVER = 10, CE_GOLD = 15;
const int WORLD_HIGH_SCORE = 38900; // 38900
const float CLUTCH_WINDOW = 2.f;
const int ACHIEVEMENT_ICON_SPACING = 132;
const int MENU_TEXT_SIZE = 64;

#endif