#ifndef DEFENCEBLOCK_HPP
#define DEFENCEBLOCK_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class DefenceBlock : public GameSprite
{
public:
    DefenceBlock(InvadersGame* mother, float x, float y);
    ~DefenceBlock() {}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
    virtual bool hitBy(GameSprite*);
    void damagedByBullet(GameSprite* bullet);
    void cropTop(int rows);
private:
    int rowsCropped;
    sf::Image image;
    sf::Texture texture;
    sf::Sprite sprite;
};

#endif
