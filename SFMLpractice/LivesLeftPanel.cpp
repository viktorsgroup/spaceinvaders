#include "LivesLeftPanel.hpp"
#include "InvadersGame.hpp"
#include "BaseSprite.hpp"

LivesLeftPanel::LivesLeftPanel(Screen* m, float x, float y, float w, float h)
    : GameSprite(m, x, y)
{
    lLeft = 3;

    setDimensions(w, h);
    BaseSprite::setupBaseShape(lifeShape);
    lifeShape.setFillColor(sf::Color::Yellow);
}

void LivesLeftPanel::display(sf::RenderWindow& App)
{
    for (int i = 0; i < lLeft; i++) {
        lifeShape.setPosition(xPos() - ((BASE_WIDTH + 8.f) * i), yPos());
        App.draw(lifeShape);
    }
}

void LivesLeftPanel::looseALife() {
    lLeft--;
}

void LivesLeftPanel::gainALife() {
    lLeft++;
}

void LivesLeftPanel::restore() {
    lLeft = 3;
}
