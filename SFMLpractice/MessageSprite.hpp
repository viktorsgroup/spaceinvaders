#ifndef MESSAGESPRITE_HPP
#define MESSAGESPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class Screen;

class MessageSprite : public GameSprite
{
public:
	MessageSprite(Screen* mother, const string& _message,
						float _lettersPerSecond = 0.3);
	~MessageSprite() {}

	virtual void setPosition(float x, float y);
	virtual void setFontSize(int size);
	virtual void display(sf::RenderWindow& App);
	virtual void timerTick(float dt);
	void setBoxColor(sf::Color color);
	void addChar(char ch);
	void removeChar();
	string getString();


protected:

private:
	void adjustBoxSize();

	sf::RectangleShape box;
	sf::Text text;
	sf::Font font;
	string message;

	float timer;
	int lettersDisplayed;
	float lettersPerSecond;
};

#endif