#include "Screen.hpp"
#include "GameSprite.hpp"
#include "Resources.hpp"

Screen::Screen() : ScreenBasic()
{
    fullscreenNeedsUpdating = false;
    changeScreen = false;
    screenQueued = EXIT;    //kinda pointless
    isFrozen = false;
    countdown = -1;
    freezeException = nullptr;

    getSettingsData();
    setupViews();
}

void Screen::updateFullscreen(sf::RenderWindow& App)
{
    if (isFullscreen) {
        App.create(sf::VideoMode(sf::VideoMode::getDesktopMode()),
            "Cosmic Conquerors", sf::Style::Fullscreen);
        App.setFramerateLimit(200);
        App.setMouseCursorVisible(false);
    }
    else {
        App.create(sf::VideoMode(WINDOWX, WINDOWY), "Cosmic Conquerors");
        App.setFramerateLimit(200);
        App.setMouseCursorVisible(true);
    }
    adjustMainViewRatio(App);
    Resources::generateBackgroundTexture(App);
    backgroundSprite->setTexture(*Resources::texture("background"), true);
    fullscreenNeedsUpdating = false;
}

void Screen::adjustMainViewRatio(sf::RenderWindow& App)
{
    float winw = float(WINDOWX) / App.getSize().x;
    float winh = float(WINDOWY) / App.getSize().y;
    float winx = (1.f - winw) / 2.f;
    float winy = (1.f - winh) / 2.f; 

    mainView->setViewport(sf::FloatRect(winx, winy, winw, winh));
    background->setSize(App.getSize().x, App.getSize().y);
}

ScreenCode Screen::run(sf::RenderWindow& App)
{
    sf::Clock clock;
    sf::Event event;

    float dt;
    changeScreen = false;
    adjustMainViewRatio(App);

    display(App);
    clock.restart();  // set to zero

    while (!changeScreen)
    {
        while (App.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                return (EXIT);
            }

            if (event.type == sf::Event::Resized) {
                adjustMainViewRatio(App);
                Resources::generateBackgroundTexture(App);
                backgroundSprite->setTexture(
                    *Resources::texture("background"), true);
            }

            if (event.type == sf::Event::KeyPressed) {
                keyPressedEvent(event.key.code);
            }

            if (event.type == sf::Event::KeyReleased) {
                keyReleasedEvent(event.key.code);
            }
        }
        sf::sleep(sf::milliseconds(16));
        dt = clock.restart().asSeconds();
        //dt = 0.016f;          //debug magic
        timerTick(dt);
        if (fullscreenNeedsUpdating) {
            updateFullscreen(App);
        }
        executeSprites();
        display(App);
        if (changeScreen == true) {
            return screenQueued;
        }
    }
    cout << "This should never happen!";
}

void Screen::timerTick(float dt)
{
    if (isFrozen && countdown >= 0) {
        countdown -= dt * 1000;
        if (countdown <= 0) {
            unfreeze();
            freezeTimeout(freezeException);
        }
    }
    if (!isFrozen) {
        for (list<GameSprite*>::iterator it = sprites.begin(); it != sprites.end(); ++it) {
            (*it)->timerTick(dt);
        }
    } else {
        if (freezeException != nullptr) {
            freezeException->timerTick(dt);
        }
    }
}

void Screen::display(sf::RenderWindow& App)
{
    App.clear();
    App.setView(*background);
    backgroundSprite->setPosition(App.mapPixelToCoords(sf::Vector2i(0, 0)));
    App.draw(*backgroundSprite);
    App.setView(*mainView);
    App.draw(*blackSquare);

    for (list<GameSprite*>::iterator it = sprites.begin() ; it != sprites.end() ; ++it) {
        (*it)->display(App);
    }
    App.display();
}

void Screen::addSprite(GameSprite*gs)
{
    sprites.push_back(gs);
}

void Screen::killSprite(GameSprite* gs)
{
    bool duplicate = false;
    for (list<GameSprite*>::iterator it = deathRow.begin(); it != deathRow.end(); ++it) {
        if (gs == *it) {
            duplicate = true;
        }
    }
    if (!duplicate) {
        deathRow.push_back(gs);
    }
}

void Screen::executeSprites()
{
    for (list<GameSprite*>::iterator it = deathRow.begin(); it != deathRow.end(); ++it) {
        sprites.remove(*it);
        delete *it;
    }
    deathRow.clear();
}

void Screen::freeze(int milliseconds, GameSprite* exception)
{
    isFrozen = true;
    countdown = milliseconds;
    freezeException = exception;
}

void Screen::unfreeze()
{
    isFrozen = false;
}

bool Screen::isInFreeze()
{
    return isFrozen;
}

void Screen::removeSprite(GameSprite* sprite)
{
    sprites.remove(sprite);
    delete sprite;
}

void Screen::setupViews()
{
    float tempw = sf::VideoMode::getDesktopMode().width;
    float temph = sf::VideoMode::getDesktopMode().height;

    float winw = WINDOWX / tempw;
    float winh = WINDOWY / temph;
    float winx = (1.f - winw) / 2.f;
    float winy = (1.f - winh) / 2.f;

    mainView = new sf::View(sf::FloatRect(0.f, 0.f, WINDOWX, WINDOWY));
    mainView->setViewport(sf::FloatRect(winx, winy, winw, winh));

    background = new sf::View(sf::FloatRect(0.f, 0.f, tempw, temph));
    background->setViewport(sf::FloatRect(0, 0, 1, 1));

    backgroundSprite = new sf::Sprite();
    backgroundSprite->setPosition(0.f, 0.f);
    backgroundSprite->setOrigin(0.f, 0.f);
    backgroundSprite->setTexture(*Resources::texture("background"), true);

    blackSquare = new sf::RectangleShape;
    blackSquare->setPosition(0.f, 0.f);
    blackSquare->setSize(sf::Vector2f(WINDOWX, WINDOWY));
    blackSquare->setFillColor(sf::Color(0, 0, 0, 255));
}

void Screen::setMuteMusic(bool on)
{
    if (on) {
        Resources::music("menu_music")->setVolume(0.f);
        Resources::music("game_music1")->setVolume(0.f);
        Resources::music("game_music2")->setVolume(0.f);
        Resources::music("game_music3")->setVolume(0.f);
    }
    else {
        Resources::music("menu_music")->setVolume(10.f);
        Resources::music("game_music1")->setVolume(15.f);
        Resources::music("game_music2")->setVolume(15.f);
        Resources::music("game_music3")->setVolume(15.f);
    }
}

void Screen::setMuteSFX(bool on)
{
    if (on) {
        Resources::sound("pew")->setVolume(0.f);
        Resources::sound("boom")->setVolume(0.f);
        Resources::sound("1-up")->setVolume(0.f);
        Resources::sound("boop")->setVolume(0.f);
        Resources::sound("select")->setVolume(0.f);
    }
    else {
        Resources::sound("pew")->setVolume(10.f);
        Resources::sound("boom")->setVolume(10.f);
        Resources::sound("1-up")->setVolume(30.f);
        Resources::sound("boop")->setVolume(10.f);
        Resources::sound("select")->setVolume(20.f);
    }
}

void Screen::getSettingsData()
{
    isFullscreen = false;
    isMusicMuted = false;
    isSFXMuted = false;

    ifstream saveFile("save.txt");
    if (saveFile.is_open()) {
        saveFile >> isFullscreen;       saveFile.ignore();
        saveFile >> isMusicMuted;       saveFile.ignore();
        saveFile >> isSFXMuted;
    }
    setMuteMusic(isMusicMuted);
    setMuteSFX(isSFXMuted);
}
