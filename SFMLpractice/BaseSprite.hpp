#ifndef BASESPRITE_HPP
#define BASESPRITE_HPP

#include "GameSprite.hpp"
#include "InvadersGlobal.hpp"

class InvadersGame;

class BaseSprite : public GameSprite
{
public:
    BaseSprite(InvadersGame* mother, float x, float y, float w, float h);
    ~BaseSprite() {}

    virtual void setPosition(float, float);

    virtual void display(sf::RenderWindow& App);
    static void setupBaseShape(sf::ConvexShape & cs);
    void setColor(float r, float g, float b, float a);
protected:
    virtual void wallHit(WallType);
private:
    sf::ConvexShape baseShape;
};

#endif
