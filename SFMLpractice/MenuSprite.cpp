#include "MenuSprite.hpp"
#include "InvadersGame.hpp"
#include "Resources.hpp"

MenuSprite::MenuSprite(Screen* mother, float x, float y,
                        float w, float h, string imagename)
    : GameSprite(mother, x, y)
{
    sprite.setScale(sf::Vector2f(4.f, 4.f));
    setDimensions(w, h);
    setPosition(x, y);

    sprite.setTexture(*Resources::texture(imagename));
}

void MenuSprite::setPosition(float x, float y)
{
    GameSprite::setPosition(x, y);
    sprite.setPosition(x, y);
}

void MenuSprite::display(sf::RenderWindow& App)
{
    App.draw(sprite);
}

void MenuSprite::rotateSprite(char direction)
{
    switch (direction)
    {
    case 'r':   //right
        sprite.rotate(90);
        sprite.setOrigin(sf::Vector2f(0, height() / 4.f));
        sprite.setPosition(xPos(), yPos());
        break;
    case 'l':   //left
        sprite.rotate(-90);
        sprite.setOrigin(sf::Vector2f(width() / 4.f, 0));
        sprite.setPosition(xPos(), yPos());
        break;
    case 'd':   //down (180 degrees)
        sprite.rotate(180);
        sprite.setOrigin(sf::Vector2f(width() / 4.f, height() / 4.f));
        sprite.setPosition(xPos(), yPos());
        break;
    default:
        break;
    }
}

void MenuSprite::setColor(sf::Color color)
{
    sprite.setColor(color);
}