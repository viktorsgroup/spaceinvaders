#ifndef GAMESPRITE_HPP
#define GAMESPRITE_HPP

#include "InvadersGlobal.hpp"

class Screen;

class GameSprite
{
public:
    GameSprite(Screen*,float x, float y);
    virtual ~GameSprite(){}

    float xPos(){return x;}
    float yPos(){return y;}
    float width() {return w;}
    float height(){return h;}
    virtual float xVelocity(){return xVel;}
    float yVelocity(){return yVel;}

    virtual void setPosition(float, float);
    virtual void setVelocity(float, float);
    virtual bool hitBy(GameSprite*);
protected:
    friend class Screen;
    Screen* mother() { return mom; }

    enum WallType { TOP_WALL, BOTTOM_WALL, LEFT_WALL, RIGHT_WALL };

    virtual void timerTick(float dt);
    virtual void display(sf::RenderWindow& App) = 0;
    virtual void wallHit(WallType);

    void setX(float _x) { x = _x; }
    void setY(float _y) { y = _y; }

    void setDimensions(float _w, float _h) {w = _w; h = _h;}
private:
    float x, y;
    float w, h;
    float xVel, yVel;
    Screen*mom;
};

#endif